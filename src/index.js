import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter } from "react-router-dom";



export const PRIMARY = "#112B99";
export const SECONDARY = "#C3EFFF";
const theme = createMuiTheme({
    palette: {
        primary: {
            main: PRIMARY,
            contrastText: "#ffffff"
        },
        secondary: {
            main: SECONDARY
        },
        error: {
            main: "#ffffff"
        }
    }
});

ReactDOM.render(
    <BrowserRouter>
        <ThemeProvider theme = {theme}>
            <App />
        </ThemeProvider>
    </BrowserRouter>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
