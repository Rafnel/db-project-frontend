import React from 'react';
import { observer, Provider } from 'mobx-react';
import TabbedMenu from './components/menu/tabbedMenu';
import AppStateStore from './config/stateClasses/appStateStore';
import PageStateStore from './config/stateClasses/pageStateStore';
import ColorSchemeStateStore from './config/stateClasses/colorSchemeStateStore';
import NotificationStateStore from './config/stateClasses/notificationStateStore';
import ErrorMessage from './components/message/ErrorMessage';
import SuccessMessage from './components/message/SuccessMessage';
import Routes from './components/pages/Routes';
import AnalyticsStateStore from './config/stateClasses/analyticsStateStore';
import CSVStateStore from './config/stateClasses/csvStateStore';
import experimentStateStore from './config/stateClasses/experimentStateStore';

const App = observer(class App extends React.Component {
	render() {
		return (
			<Provider appStateStore={new AppStateStore()}
				notificationStateStore={new NotificationStateStore()}
				pageStateStore={new PageStateStore()}
				colorSchemeStateStore={new ColorSchemeStateStore()}
				analyticsStateStore={new AnalyticsStateStore()}
				csvStateStore={new CSVStateStore()}
				experimentStateStore={new experimentStateStore()}>
				<TabbedMenu />
				<Routes />
				<ErrorMessage />
				<SuccessMessage />
			</Provider>
		);
	}
});


export default App;
