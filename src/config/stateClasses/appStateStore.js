import { observable, decorate } from 'mobx';

//overall app state items (backend url, maybe other things?)
export default class AppStateStore{
    backendURL = "";
    appBarHeight = 0;

    constructor(){
        //set the backend URL of the program
        if(window.location.hostname === "localhost"){
            this.backendURL = "http://localhost:5000";
        }
        else{
            //we aren't running locally
            this.backendURL = "https://db-backend-baylor.herokuapp.com";
        }
    }
}

//decorate this state store to be observable...
decorate(AppStateStore, {
    backendURL: observable,
    appBarHeight: observable
});