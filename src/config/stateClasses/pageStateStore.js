import { observable, decorate, action } from 'mobx';

//state store for an easy indicator to show what page we are on.
export default class PageStateStore{

    // Home Page = 0, Enter Data CSV = 1, Enter Data Manually = 2, View Data Aggregations = 3
    currentPage = 0;

    setCurrentPage(currentPage){
        console.assert(currentPage > -1 && currentPage < 4, 'current page value is invalid');
        this.currentPage = currentPage;
    }

}

//decorate this state store to be observable...
decorate(PageStateStore, {
    currentPage: observable,
    setCurrentPage: action //actions are any functions that CHANGE our OBSERVABLE state variables.
});