import { observable, decorate } from 'mobx';

export default class CSVStateStore{
    fileUploaded = null;
    finishedUpload = false;
    csvInputComponentHeight = 0;
    conditions = [];
    measurements = [];
    sequences = [];

	resultsObject = {
		loading: false,
		data: null
	}
}

//decorate this state store to be observable...
decorate(CSVStateStore, {
    fileUploaded: observable,
    finishedUpload: observable,
    resultsObject: observable,
    csvInputComponentHeight: observable,
    conditions: observable,
    measurements: observable,
    sequences: observable
});