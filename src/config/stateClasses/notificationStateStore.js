import { observable, decorate, action } from 'mobx';

//state store for all success / error notifications to user.
export default class NotificationStateStore{
    successNotification = "";
    errorNotification = "";

    setSuccessNotification(notification){
        this.successNotification = notification;
    }

    setErrorNotification(notification){
        this.errorNotification = notification;
    }
}

//decorate this state store to be observable...
decorate(NotificationStateStore, {
    successNotification: observable,
    errorNotification: observable,
    setSuccessNotification: action, //actions are any functions that CHANGE our OBSERVABLE state variables.
    setErrorNotification: action
});