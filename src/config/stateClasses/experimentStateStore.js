import { observable, decorate } from 'mobx';

export default class experimentStateStore {
	sequence = {
		name: "",
		location: "",
		description: ""
	};
	conditions = {
		checkedConditions: [],
		conditionsValues: []
	}
	measurements = {
		checkedMeasurements: [],
		measurementsValues: []
	}
	resultsObject = {
		loading: false,
		data: null
	}
}

decorate(experimentStateStore, {
	sequence: observable,
	conditions: observable,
	measurements: observable,
	resultsObject: observable
});