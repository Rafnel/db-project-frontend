import { observable, decorate, action } from 'mobx';

//state store for all success / error notifications to user.
export default class ColorSchemeStateStore{
    BackgroundColor = "#FFFFFF";
    ButtonColor = "blue";
    ButtonTextColor = "#FFFFFF"
    FontColor = '#000000';

    setLightMode(){
        this.BackgroundColor = '#FFFFFF';
        this.ButtonColor = "blue";
        this.ButtonTextColor = "#000000";
        this.FontColor = '#FFFFFF';
    }

    setDarkMode(){
        this.BackgroundColor = '#000000';
        this.ForgrounfCOlor = '#FFFFFF';
    }
}

//decorate this state store to be observable...
decorate(ColorSchemeStateStore, {
    BackgroundColor: observable,
    ForgroundColor: observable,
    setLightMode: action, //actions are any functions that CHANGE our OBSERVABLE state variables.
    setDarkMode: action
});