import { observable, decorate, action } from 'mobx';

//state store for all success / error notifications to user.
export default class AnalyticsStateStore{
    loadedExperiments = false;
    loadingExperiments = false;
    loadedTableExperiments = false;
    experiments = [];
    allConditions = [];
    allSequences = [];
    allMeasurements = [];
    allExperiments = [];
    tableExperiments = [];
    loadingAllSequences = false;
    loadingAllConditions = false;
    loadingAllMeasurements = false;
    measurements = [];

    setLoadingAllConditions(flag){
        this.loadingAllConditions = flag;
    }

    setLoadedTableExperiments(flag){
        this.loadedTableExperiments = flag;
    }

    setTableExperiments(flag){
        this.tableExperiments = flag;
    }

    setLoadingAllMeasurements(flag){
        this.loadingAllMeasurements = flag;
    }

    setAllMeasurements(flag){
        this.allMeasurements = flag;
    }

    setMeasurements(flag){
        this.measurements = flag;
    }

    setAllExperiments(flag){
        this.allExperiments = flag;
    }

    setLoadingAllSequences(flag){
        this.loadingAllSequences = flag;
    }

    setAllSequences(seqs){
        this.allSequences = seqs;
    }

    setAllConditions(conds){
        this.allConditions = conds;
    }

    setExperiments(exps){
        this.experiments = exps;
    }

    setLoadingExperiments(flag){
        this.loadingExperiments = flag;
    }

    setLoadedExperiments(flag){
        this.loadedExperiments = flag;
    }
}

//decorate this state store to be observable...
decorate(AnalyticsStateStore, {
    loadedExperiments: observable,
    loadingExperiments: observable,
    experiments: observable,
    allConditions: observable,
    allSequences: observable,
    allExperiments: observable,
    tableExperiments: observable,
    measurements: observable,
    loadingAllConditions: observable,
    loadedTableExperiments: observable,
    loadingAllSequences: observable,
    setLoadingAllConditions: action,
    setLoadingAllSequences: action,
    setTableExperiments:action,
    setAllSequences: action,
    setAllConditions: action,
    setExperiments: action,
    setLoadedExperiments: action,
    setLoadingExperiments: action,
    setMeasurements: action,
    setAllMeasurements: action,
    setLoadingAllMeasurements: action,
    setLoadedTableExperiments: action
});