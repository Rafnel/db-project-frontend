import axios from 'axios';

//returns the experiment list from the database that matches these (sequence_name, conditions_list)
export async function getExperimentsWithData(sequence_name, conditions_list, condition_values, backendURL) {
	const response = await axios.get(backendURL + "/get-experiments", {
		params: {
			sequence_name: sequence_name,
			condition_names: conditions_list,
			condition_values: condition_values
		}
	});

	return response.data;
}

export async function getExtraCredExperiments(sequence_names, condition_names, condition_values, backendURL) {
	const response = await axios.get(backendURL + "/get-extra-cred-experiments-info", {
		params: {
			sequence_names: sequence_names,
			condition_names: condition_names,
			condition_values: condition_values
		}
	});

	return response.data;
}

export async function getMeasurementsData(experiment_ids, measurement_names, backendURL){
    const response = await axios.get(backendURL + "/get-measurements-from-experiments", {
        params: {
            experiment_ids: experiment_ids,
            measurement_names: measurement_names
        }
    });

	return response.data;
}

export async function getTwoExperiments(experimentID1, experimentID2, backendURL){
    const response = await axios.get(backendURL + "/compare-two-experiments", {
        params: {
            experiment_1_id: experimentID1,
            experiment_2_id: experimentID2
        }
    });

	return response.data;
}

export async function getAllConditions(backendURL) {
	const response = await axios.get(backendURL + "/get-all-conditions");
	if (response.status !== 200 || response.status !== 201) {
		response.success = false;
	} else {
		response.success = true;
	}
	return response.data;
}

export async function getAllSequences(backendURL) {
	const response = await axios.get(backendURL + "/get-all-sequences");
	return response.data;
}


export async function getAllMeasurements(backendURL) {
	const response = await axios.get(backendURL + "/get-all-measurements");
	return response.data;
}

export async function getAllExperiments(backendURL) {
	const response = await axios.get(backendURL + "/get-all-experiments");
	return response.data;
}

export async function getMeasurementsFromExperiments(experiment_ids, measurements, backendURL) {
	const response = await axios.get(backendURL + "/get-measurements-from-experiments", {
		params: {
			experiment_ids: experiment_ids,
			measurement_names: measurements
		}
	});
	return response.data;
}


export async function getConditionValuesWithName(condition_name, backendURL) {
	const response = await axios.get(backendURL + "/get-condition-values-with-name", {
		params: {
			condition_name: condition_name
		}
	});
	return response.data;
}
