import axios from 'axios';

export async function createSequence(backend_url, sequence_name, sequence_location, sequence_description) {
	const reqObj = {
		sequence_name: sequence_name,
		sequence_location: sequence_location,
		sequence_description: sequence_description
	}
	try {
		await axios.post(backend_url + "/sequence", reqObj);
		//request was good if we made it here.
		return true;
	}
	catch (e) {
		return false;
	}
}

export async function createExperiment(sequence, conditions, measurements, backendURL){
	const reqObj = {
		sequence_name: sequence.name,
		sequence_location: sequence.location,
		sequence_description: sequence.description,
		condition_names: conditions.checkedConditions,
		condition_values: conditions.conditionsValues,
		measurement_names: measurements.checkedMeasurements,
		measurement_values: measurements.measurementsValues
	}
	const response = await axios.post(backendURL + "/experiment", reqObj);
	/*
	response.data will look like:
	{
		text: "some message",
		success: true/false (boolean)
	}
	*/
	return response.data;
}

export async function createConditionFields(backend_url, condition_name, condition_domain) {
	const reqObj = {
		condition_name: condition_name,
		condition_domain: condition_domain
	}
	try {
		await axios.post(backend_url + "/condition_fields", reqObj);
		//request was good if we made it here.
		return true;
	} catch (e) {
		return JSON.stringify(e);
	}
}

export async function createMeasurementFields(backend_url, measurement_name, measurement_domain) {
	const reqObj = {
		measurement_name: measurement_name,
		measurement_domain: measurement_domain
	}
	try {
		await axios.post(backend_url + "/measurement_fields", reqObj);
		//request was good if we made it here.
		return true;
	} catch (e) {
		return false;
	}
}

export async function addMeasurementsForExperiment(backend_url, experiment_id, measurements){
	const req_obj = {
		experiment_id: experiment_id,
		measurements: measurements
	}
	try{
		const response = await axios.post(backend_url + "/measurement-values", req_obj);
		return response.data;
	}
	catch(e){
		return false;
	}
}

export async function importCSVData(backend_url, data) {
	const reqObj = {
		data: data
	}
	try {
		let response = await axios.post(backend_url + "/import-csv", reqObj);
		response.success = true;
		return response;
	}
	catch (e) {
		const response = {
			success: false,
			data: JSON.stringify(e)
		}
		return response;
	}
}