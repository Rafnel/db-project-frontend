import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createMeasurementFields } from "../../config/api/createAPI";
import { getAllMeasurements } from "../../config/api/getAPI";

const InsertMeasurement = inject("appStateStore", "notificationStateStore", "csvStateStore")(observer(class InsertMeasurement extends React.Component {
	measurementName = "";
	measurementDomain = 'string';
	loading = false;

	constructor(props) {
		super(props);

		decorate(this, {
			loading: observable,
			measurementDomain: observable
		})
	}

	closeDialog = () => {
		this.props.modalState.isOpen = false;
	}

	handleSaveClick = async () => {
		this.loading = true;
		if(this.measurementName === ""){
			this.props.notificationStateStore.setErrorNotification("Please enter a name for your measurement.");
			this.loading = false;
			return;
		}
		if(this.measurementName.includes("_")){
			this.props.notificationStateStore.setErrorNotification("Please do not include underscores in your measurement name.");
			this.loading = false;
			return;
		}
		if(this.measurementName.includes("'") || this.measurementName.includes("\"")){
			this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in your text.");
			this.loading = false;
			return;
		}
		const response = await createMeasurementFields(this.props.appStateStore.backendURL, this.measurementName, this.measurementDomain);
		if (response !== true) {
			this.props.notificationStateStore.setErrorNotification("Measurement with name \" " + this.measurementName + " \" already exists. Could not add this measurement.");
		}
		else {
			this.props.notificationStateStore.setSuccessNotification("Measurement \" " + this.measurementName + " \" was successfully added to the database!");
		}
		this.getMeasurementNames();
		this.props.modalState.isOpen = false;
		this.loading = false;

		//get the new list of measurements for the import csv page.
		const conds = await getAllMeasurements(this.props.appStateStore.backendURL);
		this.props.csvStateStore.measurements = conds.measurements;
	}

	getMeasurementNames = async () => {
		this.props.modalState.loading = true;
		this.props.modalState.measurements = [];
		const response = await getAllMeasurements(this.props.appStateStore.backendURL);
		response.measurements.forEach(measurement => {
			this.props.modalState.measurements.push({
				name: measurement[0]
			})
		})
		this.props.modalState.loading = false;
	}

	handleChange = name => event => {
		this.measurementDomain = event.target.value;
	};

	render() {
		return (
			<div>
				<Dialog open={this.props.modalState.isOpen} onClose={this.closeDialog} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Add Measurement</DialogTitle>
					<DialogContent>
						<DialogContentText>
							Please enter the measurement name and domain type below.
						</DialogContentText>
						<TextField
							style={{ width: "100%" }}
							variant="outlined"
							margin="dense"
							label="Measurement Name"
							onChange={(event) => this.measurementName = event.target.value}
							onKeyPress={(event) => {
								if (event.key === "Enter") {
									this.handleSaveClick();
								}
							}}
						/>
						<FormControl style={{ width: "100%", display: "flex", flexDirection: "column", justify: "center", textAlign: "center" }} variant="filled" >
							<InputLabel htmlFor="filled-age-native-simple">Measurement Domain</InputLabel>
							<Select
								native
								value={this.measurementDomain}
								onChange={this.handleChange('domain')}
								inputProps={{
									name: 'domain',
									id: 'filled-age-native-simple',
								}}
							>
								<option value={'string'}>Text</option>
								<option value={'integer'}>Whole Number</option>
								<option value={'floating_point'}>Decimal Value</option>
								<option value={'boolean'}>True / False</option>
							</Select>
						</FormControl>
					</DialogContent>
					<DialogActions>
						<Button color="primary" onClick={() => this.closeDialog()}>
							Cancel
						</Button>
						<Button
							variant = "contained"
							color="primary"
							disabled = {this.loading}
							onClick={this.handleSaveClick}
						>
							<SaveIcon /> &nbsp; Save Measurement
                		</Button>
					</DialogActions>
				</Dialog>
			</div>
		)
	}
}));

export default InsertMeasurement;