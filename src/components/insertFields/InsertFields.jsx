import { Button, Grid } from "@material-ui/core";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from 'react';
import ExistingConditions from "../csvInputComponents/sidebar/ExistingConditions";
import ExistingMeasurements from "../csvInputComponents/sidebar/ExistingMeasurements";
import InsertCondition from './insertConditionFields';
import InsertMeasurement from './insertMeasurementFields';
import InsertSequenceDialog from "./InsertSequenceDialog";
import ExistingSequences from "../csvInputComponents/sidebar/ExistingSequences";

const InsertFields = inject("appStateStore")(observer(class InsertFields extends React.Component {
	showMeasurementForm = false;
	conditionModalState = {
		isOpen: false
	}
	measurementModalState = {
		isOpen: false
	}
	sequenceModalState = {
		isOpen: false
	}

	constructor(props) {
		super(props);

		decorate(this, {
			showMeasurementForm: observable,
			conditionModalState: observable,
			measurementModalState: observable,
			sequenceModalState: observable
		})
	}

	onConditionAdded = () => {
		this.conditionModalState.isOpen = true;
	}

	onMeasurementAdded = () => {
		this.measurementModalState.isOpen = true;
	}

	onSequenceAddClick = () => {
		this.sequenceModalState.isOpen = true;
	}

	render() {
		const screenHeight = window.innerHeight - this.props.appStateStore.appBarHeight;
		return (
			<div style = {{height: screenHeight, width: "100%", overflowY: "scroll", overflowX: "hidden"}}>
				<Grid style = {{paddingTop: 10}} direction = "column" container alignItems="center" justify="center" spacing = {2}>
					<Grid item>
						<Button style = {{textTransform: "initial", width: "20vw"}} variant="outlined" color="primary" onClick={() => this.onSequenceAddClick()} >
							Enter New Sequence
						</Button>
					</Grid>

					<Grid item>
						<Button style = {{textTransform: "initial", width: "20vw"}} variant="outlined" color="primary" onClick={() => this.onConditionAdded()} >
							Enter New Condition
						</Button>
					</Grid>

					<Grid item>
						<Button style = {{textTransform: "initial", width: "20vw"}} variant="outlined" color="primary" onClick={() => this.onMeasurementAdded()} >
							Enter New Measurement
						</Button>
					</Grid>

					<Grid item>
						<ExistingSequences/>
					</Grid>

					<Grid item>
						<ExistingConditions/>
					</Grid>

					<Grid item>
						<ExistingMeasurements/>
					</Grid>
				</Grid>
				<InsertCondition modalState={this.conditionModalState} />
				<InsertMeasurement modalState={this.measurementModalState} />
				<InsertSequenceDialog modalState = {this.sequenceModalState}/>
			</div>
		)
	}
}));

export default InsertFields;