import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createConditionFields } from "../../config/api/createAPI";
import { getAllConditions } from "../../config/api/getAPI";

const InsertCondition = inject("appStateStore", "notificationStateStore", "experimentStateStore", "csvStateStore")(observer(
	class InsertCondition extends React.Component {
		conditionName = "";
		conditionDomain = 'string';
		loading = false;

		constructor(props) {
			super(props);
			decorate(this, {
				loading: observable,
				conditionDomain: observable
			})
		}

		closeDialog = () => {
			this.props.modalState.isOpen = false;
		}

		handleSaveClick = async () => {
			this.loading = true;
			if(this.conditionName === ""){
				this.props.notificationStateStore.setErrorNotification("Please enter a name for your condition.");
				this.loading = false;
				return;
			}
			if(this.conditionName.includes("_")){
				this.props.notificationStateStore.setErrorNotification("Please do not include underscores in your condition name.");
				this.loading = false;
				return;
			}
			if(this.conditionName.includes("'") || this.conditionName.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in your text.");
				this.loading = false;
				return;
			}
			const response = await createConditionFields(this.props.appStateStore.backendURL, this.conditionName, this.conditionDomain);
			if (response !== true) {
				this.props.notificationStateStore.setErrorNotification("Condition with name: " + this.conditionName + " already exists. Could not add this condition.");
			}
			else {
				this.props.notificationStateStore.setSuccessNotification("Condition " + this.conditionName + " was successfully added to the database!");
			}
			this.getConditionNames();
			this.props.modalState.isOpen = false;
			this.loading = false;

			//new list of conditions for the import csv page
			const conds = await getAllConditions(this.props.appStateStore.backendURL);
			this.props.csvStateStore.conditions = conds.conditions;
		}

		getConditionNames = async () => {
			this.props.modalState.loading = true;
			this.props.modalState.conditions = [];
			const response = await getAllConditions(this.props.appStateStore.backendURL);
			response.conditions.forEach(condition => {
				this.props.modalState.conditions.push({
					name: condition[0]
				})
			})
			response.conditions.map((condition, index) => {
				this.props.experimentStateStore.conditions.checkedConditions.forEach(checked => {
					if (this.props.modalState.conditions[index].name === checked) {
						this.props.modalState.conditions[index].selected = true
					}
				})
			})
			this.props.modalState.loading = false;
		}

		handleChange = name => event => {
			this.conditionDomain = event.target.value;
		};

		render() {
			return (
				<div>
					<Dialog open={this.props.modalState.isOpen} onClose={this.closeDialog} aria-labelledby="form-dialog-title">
						<DialogTitle id="form-dialog-title">Add Condition</DialogTitle>
						<DialogContent>
							<DialogContentText>
								Please enter the condition name and domain type below.
						</DialogContentText>
							<TextField
								style={{ width: "100%" }}
								variant="outlined"
								margin="dense"
								label="Condition Name"
								onChange={(event) => this.conditionName = event.target.value}
								onKeyPress={(event) => {
									if (event.key === "Enter") {
										this.handleSaveClick();
									}
								}}
							/>
							<FormControl style={{ width: "100%", display: "flex", flexDirection: "column", justify: "center", textAlign: "center" }} variant="filled" >
								<InputLabel htmlFor="filled-age-native-simple">Condition Domain</InputLabel>
								<Select
									native
									value={this.conditionDomain}
									onChange={this.handleChange('domain')}
									inputProps={{
										name: 'domain',
										id: 'filled-age-native-simple',
									}}
								>
									<option value={'string'}>Text</option>
									<option value={'integer'}>Whole Number</option>
									<option value={'floating_point'}>Decimal Value</option>
									<option value={'boolean'}>True / False</option>
								</Select>
							</FormControl>
						</DialogContent>
						<DialogActions>
							<Button color="primary" onClick={() => this.closeDialog()}>
								Cancel
						</Button>
							<Button
								color="primary"
								variant = "contained"
								disabled = {this.loading}
								onClick={this.handleSaveClick}
							>
								<SaveIcon /> &nbsp; Save Condition
                		</Button>
						</DialogActions>
					</Dialog>
				</div>
			)
		}
	}));

export default InsertCondition;