import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, IconButton } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createMeasurementFields } from "../../config/api/createAPI";
import { getAllMeasurements } from "../../config/api/getAPI";
import NewSequence from "../manualInputSurfaces/NewSequence";
import CloseIcon from '@material-ui/icons/Close';

const InsertSequenceDialog = inject("appStateStore", "notificationStateStore", "csvStateStore")(observer(class InsertSequenceDialog extends React.Component {
	closeDialog = () => {
		this.props.modalState.isOpen = false;
	}

	render() {
		return (
			<div>
				<Dialog open={this.props.modalState.isOpen} onClose={this.closeDialog} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title"><IconButton style = {{float: "right"}} onClick = {this.closeDialog}> <CloseIcon/> </IconButton></DialogTitle>
					<DialogContent style = {{width: "400px"}}>
						<NewSequence/>
                    </DialogContent>
				</Dialog>
			</div>
		)
	}
}));

export default InsertSequenceDialog;