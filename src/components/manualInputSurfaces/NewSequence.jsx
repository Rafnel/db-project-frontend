import { TextField, Typography, Button } from "@material-ui/core";
import { decorate, observable } from "mobx";
import { observer, inject } from "mobx-react";
import React from 'react';
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../..";
import { createSequence } from "../../config/api/createAPI";
import { getAllSequences } from "../../config/api/getAPI";


const NewSequence = inject("appStateStore", "notificationStateStore", "csvStateStore")(observer(
	class NewSequence extends React.Component {
		sequenceName = "";
		sequenceDescription = "";
		sequenceFile = "";
		loading = false;

		constructor(props) {
			super(props);

			decorate(this, {
				loading: observable,
				sequenceName: observable,
				sequenceDescription: observable,
				sequenceFile: observable
			})
		}

		handleSaveClick = async () => {
			this.loading = true;

			if(this.sequenceName.length === 0){
				this.props.notificationStateStore.setErrorNotification("Please enter a name for your sequence.");
				this.loading = false;
				return;
			}

			if(this.sequenceName.includes("_")){
				this.props.notificationStateStore.setErrorNotification("Please do not include underscores in sequence names.");
				this.loading = false;
				return;
			}

			if(this.sequenceName.includes("'") || this.sequenceName.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in sequence names.");
				this.loading = false;
				return;
			}

			if(this.sequenceFile.includes("'") || this.sequenceFile.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in sequence file locations.");
				this.loading = false;
				return;
			}

			if(this.sequenceDescription.includes("'") || this.sequenceDescription.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in sequence descriptions.");
				this.loading = false;
				return;
			}

			const response = await createSequence(this.props.appStateStore.backendURL, this.sequenceName, this.sequenceFile, this.sequenceDescription);
			if(response === false){
				//entry failed.
				this.props.notificationStateStore.setErrorNotification("A sequence with name: " + this.sequenceName + " already exists. Please use a different name.");
			}
			else{
				this.props.notificationStateStore.setSuccessNotification("Sequence " + this.sequenceName + " was entered successfully!");
			}

			this.loading = false;

			const seqs = await getAllSequences(this.props.appStateStore.backendURL);
        	this.props.csvStateStore.sequences = seqs.sequences;
		}

		render() {
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
					<Typography variant="h6">
						Enter a New Sequence
                	</Typography>

					<TextField
						variant="outlined"
						margin="dense"
						label="Sequence Name"
						onChange={(event) => this.sequenceName = event.target.value}
					/>

					<TextField
						variant="outlined"
						margin="dense"
						label="OPTIONAL Sequence File Location"
						onChange={(event) => this.sequenceFile = event.target.value}
					/>

					<TextField
						variant="outlined"
						margin="dense"
						label="OPTIONAL Sequence Description"
						multiline
						rows={3}
						onChange={(event) => this.sequenceDescription = event.target.value}
					/>

					&nbsp;

					<Button
						disabled = {this.loading}
						variant = "contained"
						onClick = {this.handleSaveClick}
						color = "primary"
					>
						Submit
					</Button>

					{this.loading && <PulseLoader color={PRIMARY} />}
				</div>
			)
		}
	}));

export default NewSequence;



