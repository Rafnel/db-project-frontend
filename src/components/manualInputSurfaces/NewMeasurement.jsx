import { Checkbox, Fab, FormControlLabel, Grid, TextField, Tooltip, Typography } from "@material-ui/core";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from 'react';
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../..";
import { getAllMeasurements } from "../../config/api/getAPI";
import InsertMeasurement from '../insertFields/insertMeasurementFields';

const NewMeasurement = inject("appStateStore", "notificationStateStore", "experimentStateStore")(observer(
	class NewMeasurement extends React.Component {
		modalState = {
			isOpen: false,
			measurements: [],
			loading: false
		}

		constructor(props) {
			super(props);

			decorate(this, {
				modalState: observable
			})
			this.getMeasurementNames();
		}

		getMeasurementNames = async () => {
			this.modalState.loading = true;
			this.modalState.measurements = [];
			const response = await getAllMeasurements(this.props.appStateStore.backendURL);

			response.measurements.forEach(measurement => {
				let domain;
				if (measurement[1] === "integer") {
					domain = "Whole Number";
				}
				else if (measurement[1] === "string") {
					domain = "Text";
				}
				else if (measurement[1] === "boolean") {
					domain = "True / False";
				}
				else {
					domain = "Decimal Value";
				}

				this.modalState.measurements.push({
					name: measurement[0],
					domain: domain
				})
			})
			this.modalState.loading = false;
		}

		onMeasurementsAdded = () => {
			this.modalState.isOpen = true;
		}

		onMeasurementsSelected = (name) => {
			this.props.experimentStateStore.measurements.checkedMeasurements = [];
			this.modalState.measurements.forEach(measurement => {
				if (measurement.name === name) {
					measurement.selected = !measurement.selected;
				}
				if (measurement.selected) {
					this.props.experimentStateStore.measurements.checkedMeasurements.push(measurement.name);
				}
				if (!measurement.selected) {
					this.onMeasurementValueEntered(measurement.name, measurement.value);
				}
			})
		}

		onMeasurementValueEntered = (name, value) => {
			this.props.experimentStateStore.measurements.measurementsValues = [];
			this.modalState.measurements.forEach(measurement => {
				if (measurement.name === name) {
					measurement.value = value;
				}
				if (measurement.value !== undefined && measurement.selected) {
					this.props.experimentStateStore.measurements.measurementsValues.push(measurement.value);
				}
			})
		}

		renderMeasurementNames = () => {
			let render = [];
			// For each conditionName
			this.modalState.measurements.map((measurement, index) => {
				render.push(
					<div key={index}>
						<Grid style={{ width: "100%" }} item>
							<Tooltip title={measurement.domain}>
								<FormControlLabel
									control={
										<Checkbox
											variant="outlined"
											margin="dense"
											label={"Measurement Name"}
											color="primary"
											onChange={() => this.onMeasurementsSelected(measurement.name)}
										/>
									}
									label={measurement.name}
								/>
							</Tooltip>
						</Grid>
					</div>
				)
			})
			return render;
		}

		renderMeasurementValues = () => {
			let render = [];
			render.push(
				<div>
					<Grid style={{ width: "100%" }} item>
						{this.modalState.measurements.map((measurement, index) => {
							if (measurement.selected) {
								render.push(
									<FormControlLabel
										key={index}
										labelPlacement="start"
										control={
											<TextField
												variant="outlined"
												margin="dense"
												label={measurement.name}
												onChange={(event) => this.onMeasurementValueEntered(measurement.name, event.target.value)}
												onKeyPress={(event) => {
													if (event.key === "Enter") {
														this.handleSaveClick();
													}
												}}
											/>
										}
									/>
								)
							}
						})}
					</Grid>
				</div>)
			return render;
		}

		render() {
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
					<Typography variant="h6">
						Select Measurement(s)
                	</Typography>
					<Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start">
						{this.renderMeasurementNames()}
						{this.modalState.loading && <PulseLoader color={PRIMARY} />}
					</Grid>
					{/* For add measurementName */}
					{/* <Grid style={{ width: "100%" }} container alignItems="flex-end" justify="flex-end">
						<Grid item>
							<Tooltip title="Add Measurement">
								<Fab onClick={() => this.onMeasurementsAdded()} color="primary" aria-label="add">
									<AddIcon />
								</Fab>
							</Tooltip>
						</Grid>
					</Grid> */}
					<Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start">
						<Grid item>
							<Typography variant="h6">
								Set Value(s)
							</Typography>
						</Grid>
					</Grid>
					<Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start">
						{this.renderMeasurementValues()}
					</Grid>

					&nbsp;

					<InsertMeasurement modalState={this.modalState} />
				</div>
			);
		}

	}));

export default NewMeasurement;



