import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createConditionFields } from "../../../config/api/createAPI";
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../../..";

const NewConditionSurface = inject("appStateStore", "notificationStateStore", "experimentStateStore", "csvStateStore")(observer(
	class NewConditionSurface extends React.Component {
		conditionName = "";
		conditionDomain = 'string';
		loading = false;

		constructor(props) {
			super(props);
			decorate(this, {
				loading: observable,
				conditionDomain: observable
			})
		}

		handleSaveClick = async () => {
			this.loading = true;
			if(this.conditionName === ""){
				this.props.notificationStateStore.setErrorNotification("Please enter a name for your condition.");
				this.loading = false;
				return;
			}
			if(this.conditionName.includes("_")){
				this.props.notificationStateStore.setErrorNotification("Please do not include underscores in your condition name.");
				this.loading = false;
				return;
            }
            if(this.conditionName.includes("'") || this.conditionName.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in your text.");
				this.loading = false;
				return;
			}
			const response = await createConditionFields(this.props.appStateStore.backendURL, this.conditionName, this.conditionDomain);
			if (response !== true) {
				this.props.notificationStateStore.setErrorNotification("Condition with name: " + this.conditionName + " already exists. Could not add this condition.");
			}
			else {
				this.props.notificationStateStore.setSuccessNotification("Condition " + this.conditionName + " was successfully added to the database!");
			}
			this.loading = false;
		}

		handleChange = name => event => {
			this.conditionDomain = event.target.value;
		};

		render() {
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
                    <Typography variant = "h6">
                        Enter New Condition
                    </Typography>

                    <Typography>
                        Please enter the condition name and domain type below.
                    </Typography>

                    <TextField
                        style={{ width: "100%" }}
                        variant="outlined"
                        margin="dense"
                        label="Condition Name"
                        onChange={(event) => this.conditionName = event.target.value}
                        onKeyPress={(event) => {
                            if (event.key === "Enter") {
                                this.handleSaveClick();
                            }
                        }}
                    />
                    
                    &nbsp;

                    <FormControl style={{ width: "100%", display: "flex", flexDirection: "column", justify: "center", textAlign: "center" }} variant="filled" >
                        <InputLabel htmlFor="filled-age-native-simple">Condition Domain</InputLabel>
                        <Select
                            native
                            value={this.conditionDomain}
                            onChange={this.handleChange('domain')}
                            inputProps={{
                                name: 'domain',
                                id: 'filled-age-native-simple',
                            }}
                        >
                            <option value={'string'}>Text</option>
                            <option value={'integer'}>Whole Number</option>
                            <option value={'floating_point'}>Decimal Value</option>
                            <option value={'boolean'}>True / False</option>
                        </Select>
                    </FormControl>

                    &nbsp;

                    <Button
                        color="primary"
                        variant = "contained"
                        disabled = {this.loading}
                        onClick={this.handleSaveClick}
                    >
                        <SaveIcon /> &nbsp; Save Condition
                    </Button>

                    {this.loading && <PulseLoader color={PRIMARY} />}
				</div>
			)
		}
	}));

export default NewConditionSurface;