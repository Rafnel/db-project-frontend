import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography, Grid, Tooltip, FormControlLabel, Checkbox, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createConditionFields, createMeasurementFields, addMeasurementsForExperiment } from "../../../config/api/createAPI";
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../../..";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { getAllExperiments, getAllMeasurements } from "../../../config/api/getAPI";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const NewMeasurementsForExperiment = inject("appStateStore", "notificationStateStore", "experimentStateStore")(observer(
	class NewMeasurementsForExperiment extends React.Component {
		experimentName = "";
        loading = false;
        allExperiments = [];
        allMeasurements = [];
        checkedMeasurements = [];
        loadingExperiments = true;
        loadingMeasurements = true;
        entered = false;
        results;

		constructor(props) {
			super(props);
			decorate(this, {
                loading: observable,
                experimentName: observable,
                allExperiments: observable,
                loadingExperiments: observable,
                loadingMeasurements: observable,
                checkedMeasurements: observable,
                entered: observable
			})
        }
        
        returnResults = () => {
            //function will take the response message after user enters new measurements and let them know what was rejected and what was accepted.
            let resultPanels = [];

            //add the expansion panel for successful additions.
            let enteredItems = this.results.entered.map(item => <li key = {item}>{item}</li>)
            resultPanels.push(
                <Grid style = {{width: "100%", textAlign: "left"}} item key = "success">
                    <ExpansionPanel style = {{width: "100%", backgroundColor: "#d9d9d9"}}  defaultExpanded>
                        <ExpansionPanelSummary expandIcon = {<ExpandMoreIcon/>}>
                            Successfully Entered Measurements
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <ul style = {{listStyle: "inside", paddingLeft: 0}}>
                                {enteredItems}
                            </ul>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </Grid>
            );

            //add the panel for failed additions
            let failedItems = this.results.failed.map(item => <li key = {item.Measurement}>{"Measurement: " + item.Measurement + ", Value: " + item.Value + " | Reason: " + item.Reason}</li>)
            resultPanels.push(
                <Grid style = {{width: "100%", textAlign: "left"}}  item key = "failed">
                    <ExpansionPanel style = {{width: "100%", backgroundColor: "#d9d9d9"}} defaultExpanded>
                        <ExpansionPanelSummary expandIcon = {<ExpandMoreIcon/>}>
                            Failed Entries
                        </ExpansionPanelSummary>

                        <ExpansionPanelDetails>
                            <ul style = {{listStyle: "inside", paddingLeft: 0}}>
                                {failedItems}
                            </ul>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </Grid>
            );

            return resultPanels;
        }

		handleSaveClick = async () => {
            if(this.experimentName.length === 0){
                this.props.notificationStateStore.setErrorNotification("Please enter an experiment to add measurements to.");
                return;
            }
            if(this.checkedMeasurements.length === 0){
                this.props.notificationStateStore.setErrorNotification("Please enter some measurements to add for this experiment.");
                return;
            }
            //check if there are any empty measurement values
            for(let i = 0; i < this.checkedMeasurements.length; i++){
                if(this.checkedMeasurements[i].value === ""){
                    this.props.notificationStateStore.setErrorNotification("Please enter a value for the " + this.checkedMeasurements[i].name + " measurement that you checked.");
                    return;
                }
            }
            this.loading = true;

            const response = await addMeasurementsForExperiment(this.props.appStateStore.backendURL, this.experimentName, this.checkedMeasurements)
            this.results = response;

            if(response === false){
                this.props.notificationStateStore.setErrorNotification("Something went wrong when trying to add the measurements. Please try again.");
            }

            this.loading = false;
            this.entered = true;
        }
        
        renderMeasurementNames = () => {
			let render = [];
            // For each measurement
            for(let i = 0; i < this.allMeasurements.length; i++){
				if(this.allMeasurements[i][1] === "integer"){
					this.allMeasurements[i][1] = "Whole Number";
				}
				else if(this.allMeasurements[i][1] === "string"){
					this.allMeasurements[i][1] = "Text";
				}
				else if(this.allMeasurements[i][1] === "boolean"){
					this.allMeasurements[i][1] = "True / False";
				}
				else if(this.allMeasurements[i][1] === "floating_point"){
					this.allMeasurements[i][1] = "Decimal Value";
                }
                
                render.push(
                    <Grid key = {i} item>
                        <Tooltip title = {this.allMeasurements[i][1]}>
								<FormControlLabel
									control={
										<Checkbox
											variant="outlined"
											margin="dense"
											label={"Measurement Name"}
											color="primary"
											onChange={() => this.handleMeasurementClick(this.allMeasurements[i][0])}
										/>
									}
									label={this.allMeasurements[i][0]}
								/>
							</Tooltip>
                    </Grid>
                )
            }
			return render;
        }
        
        handleMeasurementClick = (name) => {
            //check if the measurement has been clicked already. if so, remove from the clicked list.
            let alreadyChecked = false;
            let toRemove;
            for(let i = 0; i < this.checkedMeasurements.length; i++){
                if(this.checkedMeasurements[i].name === name){
                    alreadyChecked = true;
                    toRemove = i;
                    break;
                }
            }
            if(alreadyChecked){
                //remove the element from the array (it is unsafe to do so in the for loop)
                this.checkedMeasurements.splice(toRemove, 1);
            }
            else{
                //we haven't already checked it, so add it.
                this.checkedMeasurements.push({
                    name: name,
                    value: ""
                });
            }
        }

		render() {
            let measurementValues = this.checkedMeasurements.map(measurement => 
                <Grid item key = {measurement.name}>
                    <TextField
                        variant="outlined"
                        margin="dense"
                        label={measurement.name}
                        onChange = {event => measurement.value = event.target.value}
                    />
                </Grid>
            );
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center", alignItems: "center" }}>
                    <Typography variant = "h6">
                        Enter New Measurement Values for an Existing Experiment
                    </Typography>

                    <Typography>
                        Please select the experiment that you'd like to enter new measurements for.
                    </Typography>

                    &nbsp;

                    <Autocomplete
                        id = "experiments"
                        inputValue = {this.experimentName}
                        options = {this.allExperiments}
                        getOptionLabel = {option => option}
                        onChange = {(event, value) => {
                            if(value === null){
                                this.experimentName = ""
                            }
                            else{
                                this.experimentName = value
                            }
                        }}
                        loading = {this.loadingExperiments}
                        style = {{width: "100%"}}
                        renderInput = {(params) => (
                            <TextField
                                {...params}
                                label = "Experiment Name"
                                onChange = {event => this.experimentName = event.target.value}
                                variant = "outlined"
                                fullWidth
                            />
                        )}
                    />

                    &nbsp;

                    <Typography>
                        Next, select the measurements and their values for this experiment.
                    </Typography>

                    <Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start" direction = "row">
						{this.renderMeasurementNames()}
					</Grid>

                    {this.loadingMeasurements && <Typography>Loading Measurements... <PulseLoader color={PRIMARY} /></Typography>}

                    <Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start" direction = "row" spacing = {1}>
						{measurementValues}
					</Grid>

                    &nbsp;

                    <Button
                        color="primary"
                        fullWidth
                        variant = "contained"
                        disabled = {this.loading || this.loadingExperiments || this.loadingMeasurements}
                        onClick={this.handleSaveClick}
                    >
                        <SaveIcon /> &nbsp; Save Measurements
                    </Button>

                    {this.loading && <PulseLoader color={PRIMARY} />}

                    &nbsp;

                    {this.entered && <Grid container spacing = {1} style = {{width: "100%"}} direction = "row"> {this.returnResults()} </Grid>}
				</div>
			)
        }
        
        async componentDidMount(){
            const experiments = await getAllExperiments(this.props.appStateStore.backendURL);
            this.allExperiments = experiments.experiments;
            this.loadingExperiments = false;

            const measurements = await getAllMeasurements(this.props.appStateStore.backendURL);
            this.allMeasurements = measurements.measurements;
            this.loadingMeasurements = false;
        }
	}));

export default NewMeasurementsForExperiment;