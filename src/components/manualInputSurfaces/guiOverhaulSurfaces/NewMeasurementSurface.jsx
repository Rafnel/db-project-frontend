import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography } from "@material-ui/core";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SaveIcon from '@material-ui/icons/Save';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { createConditionFields, createMeasurementFields } from "../../../config/api/createAPI";
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../../..";

const NewMeasurementSurface = inject("appStateStore", "notificationStateStore", "experimentStateStore", "csvStateStore")(observer(
	class NewMeasurementSurface extends React.Component {
		measurementName = "";
		measurementDomain = 'string';
		loading = false;

		constructor(props) {
			super(props);
			decorate(this, {
				loading: observable,
				measurementDomain: observable
			})
		}

		handleSaveClick = async () => {
			this.loading = true;
			if(this.measurementName === ""){
				this.props.notificationStateStore.setErrorNotification("Please enter a name for your measurement.");
				this.loading = false;
				return;
            }
            if(this.measurementName.includes("'") || this.measurementName.includes("\"")){
				this.props.notificationStateStore.setErrorNotification("Please do not include special characters such as apostrophes or quotes in your text.");
				this.loading = false;
				return;
			}
			const response = await createMeasurementFields(this.props.appStateStore.backendURL, this.measurementName, this.measurementDomain);
			if (response !== true) {
				this.props.notificationStateStore.setErrorNotification("Measurement with name: " + this.measurementName + " already exists. Could not add this measurement.");
			}
			else {
				this.props.notificationStateStore.setSuccessNotification("Measurement " + this.measurementName + " was successfully added to the database!");
			}
			this.loading = false;
		}

		handleChange = name => event => {
			this.measurementDomain = event.target.value;
		};

		render() {
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
                    <Typography variant = "h6">
                        Enter New Measurement
                    </Typography>

                    <Typography>
                        Please enter the measurement name and domain type below.
                    </Typography>

                    <TextField
                        style={{ width: "100%" }}
                        variant="outlined"
                        margin="dense"
                        label="Measurement Name"
                        onChange={(event) => this.measurementName = event.target.value}
                        onKeyPress={(event) => {
                            if (event.key === "Enter") {
                                this.handleSaveClick();
                            }
                        }}
                    />
                    
                    &nbsp;

                    <FormControl style={{ width: "100%", display: "flex", flexDirection: "column", justify: "center", textAlign: "center" }} variant="filled" >
                        <InputLabel htmlFor="filled-age-native-simple">Measurement Domain</InputLabel>
                        <Select
                            native
                            value={this.measurementDomain}
                            onChange={this.handleChange('domain')}
                            inputProps={{
                                name: 'domain',
                                id: 'filled-age-native-simple',
                            }}
                        >
                            <option value={'string'}>Text</option>
                            <option value={'integer'}>Whole Number</option>
                            <option value={'floating_point'}>Decimal Value</option>
                            <option value={'boolean'}>True / False</option>
                        </Select>
                    </FormControl>

                    &nbsp;

                    <Button
                        color="primary"
                        variant = "contained"
                        disabled = {this.loading}
                        onClick={this.handleSaveClick}
                    >
                        <SaveIcon /> &nbsp; Save Measurement
                    </Button>

                    {this.loading && <PulseLoader color={PRIMARY} />}
				</div>
			)
		}
	}));

export default NewMeasurementSurface;