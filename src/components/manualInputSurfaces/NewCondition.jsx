import { Checkbox, Fab, FormControlLabel, Grid, TextField, Tooltip, Typography } from "@material-ui/core";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from 'react';
import PulseLoader from 'react-spinners/PulseLoader';
import { PRIMARY } from "../..";
import { getAllConditions } from "../../config/api/getAPI";
import InsertCondition from '../insertFields/insertConditionFields';

const NewCondition = inject("appStateStore", "notificationStateStore", "experimentStateStore")(observer(
	class NewCondition extends React.Component {
		modalState = {
			isOpen: false,
			loading: false,
			conditions: []
		}

		constructor(props) {
			super(props);

			decorate(this, {
				modalState: observable
			})
			this.getConditionNames();
		}

		getConditionNames = async () => {
			this.modalState.loading = true;
			this.modalState.conditions = [];
			const response = await getAllConditions(this.props.appStateStore.backendURL);
			response.conditions.forEach(condition => {
				let domain;
				if (condition[1] === "integer") {
					domain = "Whole Number";
				}
				else if (condition[1] === "string") {
					domain = "Text";
				}
				else if (condition[1] === "boolean") {
					domain = "True / False";
				}
				else {
					domain = "Decimal Value";
				}
				this.modalState.conditions.push({
					name: condition[0],
					domain: domain
				})
			})
			this.modalState.isOpen = false;
			this.modalState.loading = false;
		}

		onConditionAdded = () => {
			this.modalState.isOpen = true;
		}

		onConditionSelected = (name) => {
			this.props.experimentStateStore.conditions.checkedConditions = [];
			this.modalState.conditions.forEach(condition => {
				if (condition.name === name) {
					condition.selected = !condition.selected;
				}
				if (condition.selected) {
					this.props.experimentStateStore.conditions.checkedConditions.push(condition.name);
				}
				if (!condition.selected) {
					this.onConditionValueEntered(condition.name, condition.value);
				}
			})
		}

		onConditionValueEntered = (name, value) => {
			this.props.experimentStateStore.conditions.conditionsValues = [];
			this.modalState.conditions.forEach(condition => {
				if (condition.name === name) {
					condition.value = value;
				}
				if (condition.value !== undefined && condition.selected) {
					this.props.experimentStateStore.conditions.conditionsValues.push(condition.value);
				}
			})
		}

		renderConditionNames = () => {
			let render = [];
			// For each conditionName
			this.modalState.conditions.map((condition, index) => {
				render.push(
					<div key={index}>
						<Grid style={{ width: "100%" }} item>
							<Tooltip title={condition.domain}>
								<FormControlLabel
									control={
										<Checkbox
											variant="outlined"
											margin="dense"
											label={"Condition Name"}
											color="primary"
											checked={condition.selected}
											value={condition.selected}
											onChange={() => this.onConditionSelected(condition.name)}
										/>
									}
									label={condition.name}
								/>
							</Tooltip>
						</Grid>
					</div>
				)
			})
			return render;
		}

		renderConditionValues = () => {
			let render = [];
			render.push(
				<div>
					<Grid style={{ width: "100%" }} item>
						{this.modalState.conditions.map((condition, index) => {
							if (condition.selected) {
								render.push(
									<FormControlLabel
										key={index}
										labelPlacement="start"
										control={
											<TextField
												variant="outlined"
												margin="dense"
												label={condition.name}
												onChange={(event) => this.onConditionValueEntered(condition.name, event.target.value)}
											/>
										}
									/>
								)
							}
						})}
					</Grid>
				</div>)
			return render;
		}

		render() {
			return (
				<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
					<Typography variant="h6">
						Select Condition(s)
                	</Typography>
					<Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start">
						{this.renderConditionNames()}
						{this.modalState.loading && <PulseLoader color={PRIMARY} />}
					</Grid>
					{/* For add conditionName */}
					{/* <Grid style={{ width: "100%" }} container alignItems="flex-end" justify="flex-end">
						<Grid item>
							<Tooltip title="Add Condition">
								<Fab onClick={() => this.onConditionAdded()} color="primary" aria-label="add">
									<AddIcon />
								</Fab>
							</Tooltip>
						</Grid>
					</Grid> */}
					<Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start">
						<Grid item>
							<Typography variant="h6">
								Set Value(s)
							</Typography>
						</Grid>
					</Grid>
					< Grid style={{ width: "100%" }} container alignItems="flex-start" justify="flex-start" >
						{this.renderConditionValues()}
					</Grid >

					&nbsp;
					<InsertCondition modalState={this.modalState} />
				</div >
			);
		}
	}
));

export default NewCondition;
