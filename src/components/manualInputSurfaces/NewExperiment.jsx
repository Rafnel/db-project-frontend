import { observer } from "mobx-react";
import React from 'react';
import { Typography, Grid } from "@material-ui/core";

const NewExperiment = observer(class NewExperiment extends React.Component {
    render(){
        return(
            <Grid container direction = "column" spacing = {2} justify = "center" alignItems = "center">
                <Grid item>
                    <Typography variant = "h6">
                        Begin a New Experiment
                    </Typography>
                </Grid>
            </Grid>
        )
    }
});

export default NewExperiment;



