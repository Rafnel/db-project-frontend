import React from "react";
import { SnackbarContent, IconButton, Snackbar } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { inject, observer } from "mobx-react";


const ErrorMessage = inject("notificationStateStore")(observer(class ErrorMessage extends React.Component{
    render(){
        return(
            <Snackbar 
                open = {this.props.notificationStateStore.errorNotification.length > 0}
                autoHideDuration = {6000} 
                onClose = {() => this.props.notificationStateStore.setErrorNotification("")}
            >
                <SnackbarContent 
                    style = {{backgroundColor: "#cc0000"}}
                    message = {this.props.notificationStateStore.errorNotification}
                    action = {<IconButton 
                                    color="inherit" 
                                    key = "close" 
                                    aria-label = "close" 
                                    onClick = {() => this.props.notificationStateStore.setErrorNotification("")}
                                > 
                                    <CloseIcon/>
                                </IconButton>}/>
            </Snackbar>
        )
    }
}));

export default ErrorMessage;