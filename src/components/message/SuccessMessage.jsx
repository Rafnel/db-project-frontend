import React from "react";
import { SnackbarContent, IconButton, Snackbar } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { inject, observer } from "mobx-react";


const SuccessMessage = inject("notificationStateStore")(observer(class SuccessMessage extends React.Component{
    render(){
        return(
            <Snackbar 
                open = {this.props.notificationStateStore.successNotification.length > 0}
                autoHideDuration = {6000} 
                onClose = {() => this.props.notificationStateStore.setSuccessNotification("")}
            >
                <SnackbarContent 
                    style = {{backgroundColor: "#228b22"}}
                    message = {this.props.notificationStateStore.successNotification}
                    action = {<IconButton 
                                    color="inherit" 
                                    key = "close" 
                                    aria-label = "close" 
                                    onClick = {() => this.props.notificationStateStore.setSuccessNotification("")}
                                > 
                                    <CloseIcon/>
                                </IconButton>}/>
            </Snackbar>
        )
    }
}));

export default SuccessMessage;