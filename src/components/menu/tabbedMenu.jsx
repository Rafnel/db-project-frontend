import { inject, observer } from "mobx-react";
import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withRouter } from "react-router-dom";
import { AppBar, Typography } from "@material-ui/core";

const TabbedMenu = inject("pageStateStore", "appStateStore")(observer(class TabbedMenu extends React.Component { 
  
  handleChange = (event, value) => {
    this.props.pageStateStore.setCurrentPage(value);
    if(value === 0){
      this.props.history.push("/");
    }
    else if(value === 1){
      this.props.history.push("/csv")
    }
    else if(value === 2){
      this.props.history.push("/manual-entry");
    }
    else{
      this.props.history.push("/analytics");
    }
  };

  render(){
    return (
      <AppBar ref = "appbar" color = "primary" position = "static">
        <Tabs
          value={this.props.pageStateStore.currentPage}
          onChange={this.handleChange}
          indicatorColor="secondary"
          centered
        >
          <Tab style = {{textTransform: "initial"}} label={<Typography color = "error">Home</Typography>}/> {/* makes text look the way you coded it to, not uppercase */}
          <Tab style = {{textTransform: "initial"}} label={<Typography color = "error">Enter Data With CSV File</Typography>}/>
          <Tab style = {{textTransform: "initial"}} label={<Typography color = "error">Enter Data Manually</Typography>}/>
          <Tab style = {{textTransform: "initial"}} label={<Typography color = "error">View Data Aggregations</Typography>} />
        </Tabs>
      </AppBar>
    );
  }

  componentDidMount(){
    let {clientHeight, clientWidth} = this.refs.appbar;
    this.props.appStateStore.appBarHeight = clientHeight;
  }
}));

export default withRouter(TabbedMenu);



