import { Container, FormControl, Grid, InputLabel, MenuItem, Select, Typography } from "@material-ui/core";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { getAllConditions, getAllExperiments, getAllMeasurements, getAllSequences } from "../../config/api/getAPI";
import ExperimentComparisonComponent from "../Analytics/ExperimentComparison/ExperimentComparisonComponent";
import ExperimentInfoComponent from "../Analytics/ExperimentInfoComponent";
import ViewMeasurementsTable from "../Analytics/ExtraCred/ViewMeasurementTable";
import ViewMultipleResultsExtraCred from "../Analytics/ExtraCred/ViewMultipleResultsExtraCred";

const Analytics = inject("analyticsStateStore", "appStateStore")(observer(class Analytics extends React.Component{
	selectedValue = 1;

	constructor(props){
		super(props);

		decorate(this, {
			selectedValue: observable
		})
	}

	render() {
		return (
			<Container maxWidth = "lg" fixed>
				<Grid style = {{padding: "20px"}} spacing = {2} direction = "column" justify = "center" alignItems = "center" container>
					<Grid item>
						<Typography variant = "h5">
							View data analytics
						</Typography>
					</Grid>

					<Grid item>
						<FormControl variant = "outlined" style = {{width: "35vw"}}>
							<InputLabel style = {{backgroundColor: "#ffffff"}}>Type of Analysis</InputLabel>
							<Select
								onChange = {event => this.selectedValue = event.target.value}
								defaultValue = {1}
							>
								<MenuItem value = {1}>View Information About Experiment(s)</MenuItem>
								<MenuItem value = {2}>View a Side-by-side Comparison of Two Experiments</MenuItem>
								<MenuItem value = {3}>View Results Across Multiple Experiments</MenuItem>
								<MenuItem value = {4}>View Experiments' Measurements in Table Format</MenuItem>
							</Select>
						</FormControl>
					</Grid>

					{this.selectedValue === 1 && 
						<Grid style = {{width: "100%"}} item>
							<ExperimentInfoComponent/>
						</Grid>
					}
					{this.selectedValue === 2 &&
						<Grid style = {{width: "100%"}} item>
							<ExperimentComparisonComponent/>
						</Grid>
					}
					{this.selectedValue === 3 &&
						<Grid style = {{width: "100%"}} item>
							<ViewMultipleResultsExtraCred/>
						</Grid>
					}
					{this.selectedValue === 4 &&
						<Grid style = {{width: "100%"}} item>
							<ViewMeasurementsTable/>
						</Grid>
					}
				</Grid>
			</Container>
		);
	}

	async componentDidMount(){
		this.props.analyticsStateStore.setLoadingAllSequences(true);
		this.props.analyticsStateStore.setLoadingAllConditions(true);
		this.props.analyticsStateStore.setLoadingAllMeasurements(true);

		//get all conditions from the db for the dropdown selector.
		const conds_data = await getAllConditions(this.props.appStateStore.backendURL);

		this.props.analyticsStateStore.setAllConditions(conds_data.conditions);
		this.props.analyticsStateStore.setLoadingAllConditions(false);

		//get all sequences from the db for the dropdown selector.
		const seqs_data = await getAllSequences(this.props.appStateStore.backendURL);

		this.props.analyticsStateStore.setAllSequences(seqs_data.sequences);
		this.props.analyticsStateStore.setLoadingAllSequences(false);

		const meas_data = await getAllMeasurements(this.props.appStateStore.backendURL);
		this.props.analyticsStateStore.setAllMeasurements(meas_data.measurements);
		this.props.analyticsStateStore.setLoadingAllMeasurements(false);

		const experiments = await getAllExperiments(this.props.appStateStore.backendURL);
        this.props.analyticsStateStore.setAllExperiments(experiments.experiments);
	}
}));

export default Analytics;