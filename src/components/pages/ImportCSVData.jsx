import { Divider, Grid } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React from "react";
import InputCSV from "../csvInputComponents/inputFile/InputCSV";
import CSVEnteredResults from "../csvInputComponents/inputResults/CSVEnteredResults";
import InsertFields from '../insertFields/InsertFields';

const ImportCSVData = inject("appStateStore", "notificationStateStore", "csvStateStore")(observer(class ImportCSVData extends React.Component{
	dataWidth = 0;
	render() {
		const screenHeight = window.innerHeight - this.props.appStateStore.appBarHeight;
		return (
			<div style = {{width: "100%", height: "100%", display: "flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "flex-start"}}>
				<div ref = "data" style = {{width: "76vw", flex: 1, direction: "column", justifyContent: "flex-start", alignItems: "flex-start"}}>
					<Grid item>
						<InputCSV/>
					</Grid>

					<Grid item>
						{(this.props.csvStateStore.resultsObject.loading || this.props.csvStateStore.finishedUpload) && 
							<CSVEnteredResults resultsObject = {this.props.csvStateStore.resultsObject}/>
						}
					</Grid>
				</div>

				<Divider orientation = "vertical" style = {{height: screenHeight}}/>				
				
				<div style = {{width: "24vw", height: "100%"}}>
					<InsertFields style = {{height: "100%"}}/>
				</div>
			</div>
		);
	}
}));

export default ImportCSVData;