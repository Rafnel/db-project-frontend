import { Button, Container, Fab, FormControl, Grid, Paper, TextField, Tooltip, Typography } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { decorate, observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { createExperiment } from "../../config/api/createAPI";
import { getAllSequences } from '../../config/api/getAPI';
import NewCondition from '../manualInputSurfaces/NewCondition';
import NewMeasurement from '../manualInputSurfaces/NewMeasurement';

const ImportDataManually = inject("appStateStore", "notificationStateStore", "analyticsStateStore", "experimentStateStore")(observer(
	class ImportDataManually extends React.Component {
		sequenceName = "";
		selectorValue = 1;
		loading = false;

		constructor(props) {
			super(props);

			decorate(this, {
				sequenceName: observable,
				selectorValue: observable,
				loading: observable
			})
		}

		handleSaveClick = async () => {
			this.loading = true;

			//validate the forms
			if (this.props.experimentStateStore.sequence.name === "") {
				this.props.notificationStateStore.setErrorNotification("Please enter a sequence name.");
				this.loading = false;
				return;
			}

			if (this.props.experimentStateStore.conditions.checkedConditions.length === 0) {
				this.props.notificationStateStore.setErrorNotification("Please enter some conditions for your experiment.");
				this.loading = false;
				return;
			}

			//make sure that each checked item has a non-blank value associated.
			for (let i = 0; i < this.props.experimentStateStore.conditions.conditionsValues.length; i++) {
				if (this.props.experimentStateStore.conditions.conditionsValues[i] === "") {
					this.props.notificationStateStore.setErrorNotification("Please do not leave any condition values blank.");
					this.loading = false;
					return;
				}
			}

			for (let i = 0; i < this.props.experimentStateStore.measurements.measurementsValues.length; i++) {
				if (this.props.experimentStateStore.measurements.measurementsValues[i] === "") {
					this.props.notificationStateStore.setErrorNotification("Please do not leave any measurement values blank.");
					this.loading = false;
					return;
				}
			}

			const response = await createExperiment(this.props.experimentStateStore.sequence, this.props.experimentStateStore.conditions,
				this.props.experimentStateStore.measurements, this.props.appStateStore.backendURL);

			if (response.success) {
				this.props.notificationStateStore.setSuccessNotification(response.text);
			}
			else {
				this.props.notificationStateStore.setErrorNotification(response.text);
			}
			this.loading = false;
		}

		render() {
			return (
				<Grid
					style={{ padding: '20px' }}
					spacing={2}
					direction="column"
					justify="center"
					alignItems="center"
					container
				>
					<Grid item>
						<FormControl
							variant="outlined"
							fullWidth={true}
							style={{
								minWidth: '30vw',
								display: 'flex',
								wrap: 'nowrap',
								margin: 0
							}}
						>
						</FormControl>
					</Grid>
					<Container maxWidth="md" fixed={true}>
						<Grid style={{ width: "100%" }} container direction="row" alignItems="center" justify="center">
							<Grid style={{ width: "100%" }} item>
								<Paper style={{ padding: 10 }}>
									<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
										<Typography variant="h6">
											Select a Sequence
                						</Typography>
									</div>
									<Grid item>
										<Autocomplete
											id="sequences"
											inputValue={this.props.experimentStateStore.sequence.name}
											options={this.props.analyticsStateStore.allSequences}
											getOptionLabel={option => option[0]}
											onChange={(event, value) => {
												if (value === null) {
													this.props.experimentStateStore.sequence.name = ""
												}
												else {
													this.props.experimentStateStore.sequence.name = value[0]
												}
											}}
											loading={this.props.analyticsStateStore.loadingAllSequences}
											style={{ width: "300px" }}
											renderInput={(params) => (
												<TextField
													{...params}
													label="Sequence Name"
													onChange={event => this.props.experimentStateStore.sequence.name = event.target.value}
													variant="outlined"
													fullWidth
												/>
											)}
										/>
									</Grid>
								</Paper>
							</Grid>
						</Grid>
					</Container>
					<Container maxWidth="md" fixed={true}>
						<Grid style={{ width: "100%" }} container alignItems="center" justify="center">
							<Grid style={{ width: "100%" }} item>
								<Paper style={{ padding: 10 }}>
									<NewCondition />
								</Paper>
							</Grid>
						</Grid>
					</Container>
					<Container maxWidth="md" fixed={true}>
						<Grid style={{ width: "100%" }} container alignItems="center" justify="center">
							<Grid style={{ width: "100%" }} item>
								<Paper style={{ padding: 10 }}>
									<NewMeasurement />
								</Paper>
							</Grid>
						</Grid>
					</Container>
					<Container maxWidth="md" fixed={true}>
						<Grid style={{ width: "100%" }} container alignItems="center" justify="center">
							<Grid style={{ width: "100%" }} item>
								<Paper style={{ padding: 10 }}>
									<div style={{ width: "100%", display: "flex", flexDirection: "column", textAlign: "center" }}>
										<Button
											color="primary"
											variant="contained"
											onClick={this.handleSaveClick}
											disabled={this.loading}
										>
											<SaveIcon /> &nbsp; Save Experiment
										</Button>
									</div>
								</Paper>
							</Grid>
						</Grid>
					</Container>

				</Grid>
			);
		}

		async componentDidMount() {
			this.props.analyticsStateStore.loadingAllSequences = true;
			this.props.analyticsStateStore.allSequences = (await getAllSequences(this.props.appStateStore.backendURL)).sequences;
			this.props.analyticsStateStore.loadingAllSequences = false;
		}

		componentWillUnmount(){
			this.props.experimentStateStore.sequence.name = "";
		}
	}
));

export default ImportDataManually;
