import { Button, Container, Fab, FormControl, Grid, Paper, TextField, Tooltip, Typography, InputLabel, Select, MenuItem } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import SaveIcon from '@material-ui/icons/Save';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { decorate, observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { createExperiment } from "../../config/api/createAPI";
import { getAllSequences } from '../../config/api/getAPI';
import NewCondition from '../manualInputSurfaces/NewCondition';
import NewMeasurement from '../manualInputSurfaces/NewMeasurement';
import NewSequence from '../manualInputSurfaces/NewSequence';
import NewConditionSurface from '../manualInputSurfaces/guiOverhaulSurfaces/NewConditionSurface';
import NewMeasurementSurface from '../manualInputSurfaces/guiOverhaulSurfaces/NewMeasurementSurface';
import NewMeasurementsForExperiment from '../manualInputSurfaces/guiOverhaulSurfaces/NewMeasurementsForExperiment';
import ImportDataManually from './ImportDataManually';

const EnterDataManuallyTry2 = inject("appStateStore", "notificationStateStore", "analyticsStateStore", "experimentStateStore")(observer(
	class EnterDataManuallyTry2 extends React.Component {
        selectorValue = 0;

        constructor(props){
            super(props);

            decorate(this, {
                selectorValue: observable
            });
        }

        renderDataEntrySurface = () => {
            if(this.selectorValue === 1){
                return <NewSequence/>
            }
            else if(this.selectorValue === 2){
                return <NewConditionSurface/>
            }
            else if(this.selectorValue === 3){
                return <NewMeasurementSurface/>
            }
            else if(this.selectorValue === 4){
                return <ImportDataManually/>
            }
            else if(this.selectorValue === 5){
                return <NewMeasurementsForExperiment/>
            }
        }

		render() {
			return (
                <Grid
					style={{ paddingTop: '20px' }}
					spacing={2}
					direction="column"
					justify="center"
					alignItems="center"
					container
				>
                    <Grid item>
                        <Typography variant = "h5">
                            Enter Your Data Manually
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography>
                            Select what data entry operation you would like to do with the below selector.
                        </Typography>
                    </Grid>

                    <Grid item>
                        <FormControl variant="outlined">
                            <InputLabel >
                                Operation
                            </InputLabel>
                            <Select
                                value={this.selectorValue}
                                onChange={event => this.selectorValue = event.target.value}
                                labelWidth = {80}
                                style = {{width: "500px"}}
                            >
                                <MenuItem value = {0}>
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={1}>Enter New Sequence</MenuItem>
                                <MenuItem value={2}>Enter New Condition</MenuItem>
                                <MenuItem value={3}>Enter New Measurement</MenuItem>
                                <MenuItem value={4}>Enter New Experiment</MenuItem>
                                <MenuItem value={5}>Enter New Measurement Values for an Existing Experiment</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item style = {{width: "70vw"}}>
                        <Paper style = {{padding: 10, width: "100%"}}>
                            {this.renderDataEntrySurface()}
                        </Paper>
                    </Grid>
                </Grid>
            )
        }
    }
));

export default EnterDataManuallyTry2;
