import React from 'react';
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import Analytics from './Analytics';
import HomePage from "./HomePage";
import ImportCSVData from './ImportCSVData';
import ImportDataManually from './ImportDataManually';
import { observer, inject } from 'mobx-react';
import EnterDataManuallyTry2 from './EnterDataManuallyTry2';

const Routes = inject("pageStateStore")(observer(class Routes extends React.Component{
    render(){
        return(
            <Switch>
                <Route path="/" exact render = {() => {this.props.pageStateStore.setCurrentPage(0); return <HomePage/>}} />
                <Route path="/manual-entry" exact render = {() => {this.props.pageStateStore.setCurrentPage(2); return <EnterDataManuallyTry2/>}} />
                <Route path="/csv" exact render = {() => {this.props.pageStateStore.setCurrentPage(1); return <ImportCSVData/>}} />
                <Route path="/analytics" exact render = {() => {this.props.pageStateStore.setCurrentPage(3); return <Analytics/>}} />
                { /* Finally, catch all unmatched routes */ }
                <Route render = {() => <Redirect to = "/"/>} />
            </Switch>
        )
    }
}));

export default withRouter(Routes);