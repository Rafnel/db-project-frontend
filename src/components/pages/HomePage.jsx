import React from "react";
import { observer } from "mobx-react";
import { Typography, Grid, Paper } from "@material-ui/core";

const HomePage = observer(class HomePage extends React.Component {
	state = {
		numPages: null
	}

	onLoadSuccess = ({ numPages }) => {
		this.setState({ numPages });
	}

	render() {
		const { numPages } = this.state;

		return (
			<Grid
				container
				spacing={2}
				align="center"
				alignContent="center"
				alignItems="center"
				justify="center"
				direction="column"
				style={{ padding: "20px" }}
			>
				<Grid item>
					<Typography variant="h5">Welcome to Our Website!</Typography>
				</Grid>

				<Grid style={{ textAlign: "left" }} item>
					<Paper style={{ padding: 10, width: "75vw" }}>
						<Grid container spacing={2} alignItems="flex-start" justify="flex-start" direction="column">
							<Grid item>
								<Typography variant="h5">
									User Manual
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									Zac Steudel, Meghan Bibb, Andrew Case<br />
									Database Fall 2019<br />
									Biology Department Group Project
                				</Typography>
							</Grid>

							<Grid item>
								<Typography variant="h6">
									Accessing and Using the Application
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									Our project is hosted at <a href="https://db-project-frontend.herokuapp.com/" target="_blank">https://db-project-frontend.herokuapp.com/</a>.
									After navigating to this website and you will see four tabs at the top of the screen.
									The landing page is a small home screen with three additional containing the core functionality of the application.
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									<b>CSV File Input</b>
									<br />
									The first tab after the home screen takes you to the CSV Input.
									Here, you can enter your own CSV file with the following: experiments, conditions, and measurements.
									If any errors occur pertaining to what was entered, the results of that error will be shown to the user.
									On the sidebar, the existing conditions and measurements are displayed, as well as the ability to enter new conditions or measurements.
									<br />
									NOTE: If the conditions or measurements within a CSV File do not exist prior to uploading, the data will be rejected.
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									<b>Manual Input</b>
									<br />
									The next tab is the Manual Data Input tab.
									Similarly to the CSV Input, this allows the user to enter new experiments with conditions and measurements.
									If any errors occur pertaining to what was entered, the results of that error will be shown to the user as well.
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									<b>Analysis {'&'} Aggregation</b>
									<br />
									Finally, the last tab pertains to Data Analysis and Aggregation.
									On this page, there are four options:
									<ul>
										<li><u>View Information About Experiment(s)</u></li>
										<ul>
											A user can enter information about experiment(s) such as the sequence and condition(s), then view all of the information pertaining to the experiment(s) with that set of (sequence, conditions).
										</ul>
										<br />
										<li><u>Side-by-Side Comparison of 2 Experiments</u></li>
										<ul>
											This option displays a direct, side-by-side comparision of two experiments. Upon selecting 2 existing experiments, the information about those experiments is shown for comparison.
										</ul>
										<br />
										<li><u>View Results Across Multiple Experiments</u></li>
										<ul>
											Like the Side-by-Side Comparison option, a user can select experiments to compare side-by-side, however there is no limit to how many.
										</ul>
										<br />
										<li><u>View Experiments' Measurements</u></li>
										<ul>
											This shows multiple experiments' measurements, organized within a table.
											<br />
											NOTE: An analysis from one of the previous options must be performed beforehand with the same experiments.
										</ul>
									</ul>
								</Typography>
							</Grid>

							<Grid item>
								<Typography variant="h6">
									Running the Application Locally
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									If you want to run the application locally, instead of accessing the already-running version,
									you will need to have the <a href="https://gitlab.com/Rafnel/db-project-frontend" target="_blank">frontend</a> and <a href="https://gitlab.com/Rafnel/db-project-backend" target="_blank">backend</a> files.
									<ul >
										NOTE: You will need to have <a href="https://nodejs.org/en/download/" target="_blank">Node Package Manger (NPM)</a> installed on your computer, as the frontend is a JavaScript / React app.
										You will also need <a href="https://www.python.org/downloads/" target="_blank">Python 3</a> installed (we recommend using <a href="https://www.jetbrains.com/pycharm/download/" target="_blank">PyCharm</a>), as the backend is written in Python using the Flask module.
										If you decide to not use PyCharm, you will need to use PIP to install the necessary modules (it comes with Python 3).
										Administrative access may be required to install the software previously listed.
									</ul>
									<b>Frontend Setup</b>

									<ul>
										After having installed NPM, navigate to the root directory of the frontend files.
										Once there, run the following commands: “<i>npm install</i>” and then “<i>npm start</i>” within your terminal.
										After running npm start, your default browser should open a window displaying the homepage.
									</ul>
									<b>Backend Setup</b>
									<ul>
										<u>PyCharm Usage</u>
										<br />
										After having installed Python 3, open the Python project in PyCharm, navigate to the main.py, and run it by clicking the green arrow.
										<br />
										<u>Command-Line Usage</u>
										<br />
										After having installed Python 3, run the following commands:
										<br />
										"<i>pip3 install Flask</i>", "<i>pip3 install mysql-connector</i>", and "<i>pip3 install Flask-Cors</i>".
										<br />
										Once all those modules are installed, navigate to the root director of the backend files, and run the main.py file using
										<br />
										"<i>python3 ./main.py</i>".
										This will start the Flask backend locally, which accesses the cloud-hosted MySQL database.
									</ul>
								</Typography>
							</Grid>

							<Grid item>
								<Typography variant="h6">
									Accessing the MySQL Database Tables using MySQL Workbench
                				</Typography>
							</Grid>

							<Grid item>
								<Typography>
									If you would like to access the tables using MySQL workbench to view or manipulate the data manually, you can use these credentials to attain a connection:
									<br />
									<i>Hostname: us-cdbr-iron-east-05.cleardb.net</i>
									<br />
									<i>User: b48edc3c45e2c4</i>
									<br />
									<i>Password: 5483a468</i>
									<br />
									<i>Database Name(may not be required to connect on MySQL Workbench): heroku_ff58d98d017aaaa</i>
								</Typography>
							</Grid>
						</Grid>
					</Paper>
				</Grid>
			</Grid>
		);
	}
});

export default HomePage;