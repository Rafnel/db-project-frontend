import {inject, observer} from "mobx-react";
import React from "react";
import {decorate, observable} from "mobx";
import {Grid, TextField} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";

const MeasurementInputField = inject("analyticsStateStore", "appStateStore")(observer(
	class MeasurementInputField extends React.Component {
		loadingMeasurementValues = false;

		constructor(props) {
			super(props);

			decorate(this, {
				loadingMeasurementValues: observable
			})
		}

		handleMeasurementNameChange = async (event, value) => {
			this.loadingMeasurementValues = true;
			this.valuesList = [];
			if (value === null) {
				this.props.measurements[this.props.measurementKey].name = "";
			}
			else {
				this.props.measurements[this.props.measurementKey].name = value[0];
			}

			this.loadingMeasurementValues = false;
		}

		render() {
			return (
				<Grid container direction="column" spacing={1}>
					<Grid item>
						<Autocomplete
							id={"" + this.props.measurementKey}
							inputValue={this.props.measurements[this.props.measurementKey].name}
							options={this.props.analyticsStateStore.allMeasurements}
							getOptionLabel={option => option[0]}
							onChange={this.handleMeasurementNameChange}
							loading={this.props.analyticsStateStore.loadingAllMeasurements}
							style={{ width: "250px" }}
							renderInput={(params) => (
								<TextField
									{...params}
									label="Measurement Name"
									variant="outlined"
									onChange={event => this.props.measurements[this.props.measurementKey].name = event.target.value}
									fullWidth
								/>
							)}
						/>
					</Grid>
				</Grid>
			)
		}
	}));


export default MeasurementInputField;