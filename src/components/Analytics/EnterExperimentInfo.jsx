import { Button, Grid, IconButton, TextField, Tooltip, Typography } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { getExperimentsWithData } from "../../config/api/getAPI";
import ConditionsInputField from "./ConditionsInputField";


const EnterExperimentInfo = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class EnterExperimentInfo extends React.Component {
	sequenceName = "";
	conditions = {
		conditionsList: []
	};
	conditionsTextFields = []

	constructor(props) {
		super(props);

		decorate(this, {
			conditionsTextFields: observable,
			sequenceName: observable,
			conditions: observable
		})

		//initially only one text field.
		this.handleAddCondition();
	}

	handleAddCondition = () => {
		this.conditions.conditionsList.push({ condition: "", value: "" });
		const conditionKey = this.conditionsTextFields.length;
		this.conditionsTextFields.push(
			<Grid key={this.conditionsTextFields.length} item>
				<ConditionsInputField conditionKey={conditionKey} conditions={this.conditions} />
			</Grid>
		)
	}

	handleMakeQuery = async () => {
		this.props.analyticsStateStore.loadingExperiments = true;
		let emptyConditionCounter = 0, emptyValueCounter = 0;
		let invalidPair = false;
		this.conditions.conditionsList.forEach(item => {
			if (item.condition === "") {
				emptyConditionCounter++;
			}
			if (item.value === "") {
				emptyValueCounter++;
			}
			if ((item.condition === "" && item.value !== "") || (item.condition !== "" && item.value === "")) {
				invalidPair = true;
			}
		})
		if (emptyConditionCounter == emptyValueCounter && !invalidPair) {
			const conditions = this.conditions.conditionsList.filter((item) => item.condition !== "" && item.value !== "");
			const condition_names = conditions.map(item => item.condition);
			const condition_values = conditions.map(item => item.value);
			if (condition_names.length === condition_values.length) {
				const data = await getExperimentsWithData(this.sequenceName, condition_names, condition_values, this.props.appStateStore.backendURL);
				this.props.analyticsStateStore.setExperiments(data.experiments);
				this.props.analyticsStateStore.setLoadingExperiments(false);
				this.props.analyticsStateStore.setLoadedExperiments(true);
			}
		} else {
			this.props.notificationStateStore.setErrorNotification("One or more condition/value pairs is incorrect.");
			this.props.analyticsStateStore.loadingExperiments = false;
		}
	}

	render() {
		const items = this.conditionsTextFields.map((field) => { return field });

		return (
			<Grid container direction="column" alignItems="center" justify="center" spacing={2}>
				<Grid item>
					<Typography>
						First, enter the set of (sequence, conditions) that make up your experiment.
                    </Typography>
				</Grid>

				<Grid item container direction="row" alignItems="center" justify="center" spacing={2}>
					<Autocomplete
						id="sequences"
						inputValue={this.sequenceName}
						options={this.props.analyticsStateStore.allSequences}
						getOptionLabel={option => option[0]}
						onChange={(event, value) => {
							if (value === null) {
								this.sequenceName = ""
							}
							else {
								this.sequenceName = value[0]
							}
						}}
						loading={this.props.analyticsStateStore.loadingAllSequences}
						style={{ width: "300px" }}
						renderInput={(params) => (
							<TextField
								{...params}
								label="Sequence Name"
								onChange={event => this.sequenceName = event.target.value}
								variant="outlined"
								fullWidth
							/>
						)}
					/>

					{items}

					<Grid item>
						<Tooltip title="Add Condition">
							<IconButton color="primary" onClick={this.handleAddCondition}>
								<AddIcon />
							</IconButton>
						</Tooltip>
					</Grid>
				</Grid>

				<Grid item>
					<Button
						color="primary"
						variant="contained"
						onClick={this.handleMakeQuery}
						disabled={this.props.analyticsStateStore.loadingExperiments}
						style={{ textTransform: "initial" }}
					>
						<SearchIcon /> &nbsp; Load Experiment Data
                    </Button>
				</Grid>
			</Grid>
		)
	}
}));


export default EnterExperimentInfo;