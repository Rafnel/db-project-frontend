import { Grid } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React, { Fragment } from "react";
import EnterExperimentInfo from "./EnterExperimentInfo";
import ExperimentQueryResults from "./ExperimentQueryResults";


const ExperimentInfoComponent = inject("analyticsStateStore")(observer(class ExperimentInfoComponent extends React.Component{
    render(){
        return(
            <Fragment style = {{width: "100%"}}>
                <Grid item>
                    <EnterExperimentInfo/>
                </Grid>

                {(this.props.analyticsStateStore.loadingExperiments || this.props.analyticsStateStore.loadedExperiments) && 
                    <Grid style = {{width: "100%"}} item>
                        <ExperimentQueryResults/>
                    </Grid>
                }
            </Fragment>
        )
    }
}));


export default ExperimentInfoComponent;