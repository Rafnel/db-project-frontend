import { Grid, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";

const SequencesInputField = inject("analyticsStateStore", "appStateStore")(observer(
	class SequencesInputField extends React.Component {
		loadingSequenceValues = false;

		constructor(props) {
			super(props);

			decorate(this, {
				loadingSequenceValues: observable
			})
		}

		handleSequenceNameChange = async (event, value) => {
			this.loadingSequenceValues = true;
			this.valuesList = [];
			if (value === null) {
				this.props.sequences[this.props.sequenceKey].name = "";
			}
			else {
				this.props.sequences[this.props.sequenceKey].name = value[0];
			}

			this.loadingSequenceValues = false;
		}

		render() {
			return (
				<Grid container direction="column" spacing={1}>
					<Grid item>
						<Autocomplete
							id={"" + this.props.sequenceKey}
							inputValue={this.props.sequences[this.props.sequenceKey].name}
							options={this.props.analyticsStateStore.allSequences}
							getOptionLabel={option => option[0]}
							onChange={this.handleSequenceNameChange}
							loading={this.props.analyticsStateStore.loadingAllSequences}
							style={{ width: "250px" }}
							renderInput={(params) => (
								<TextField
									{...params}
									label="Sequence Name"
									variant="outlined"
									onChange={event => this.props.sequences[this.props.sequenceKey].name = event.target.value}
									fullWidth
								/>
							)}
						/>
					</Grid>
				</Grid>
			)
		}
	}));


export default SequencesInputField;