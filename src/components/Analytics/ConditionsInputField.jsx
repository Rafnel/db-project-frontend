import { Grid, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { getConditionValuesWithName } from "../../config/api/getAPI";

function makeConditionValuesUnique(conditions){
    //conditions[2] has our value.
    let valueSet = new Set();
    let newArray = [];
    for(let i = 0; i < conditions.length; i++){
        if(!valueSet.has(conditions[i][2])){
            newArray.push(conditions[i]);
        }
        valueSet.add(conditions[i][2]);
    }
    return newArray;
}


const ConditionsInputField = inject("analyticsStateStore", "appStateStore")(observer(class ConditionsInputField extends React.Component{
    loadingConditionValues = false;
    valuesList = [];

    constructor(props){
        super(props);

        decorate(this, {
            loadingConditionValues: observable,
            valuesList: observable
        })
    }

    handleConditionNameChange = async (event, value) => {
        this.loadingConditionValues = true;
        this.valuesList = [];
        if(value === null){
            this.props.conditions.conditionsList[this.props.conditionKey].condition = "";
        }
        else{
            this.props.conditions.conditionsList[this.props.conditionKey].condition = value[0];
            //get all the values for this condition and set our values list.
            this.valuesList = (await getConditionValuesWithName(value[0], this.props.appStateStore.backendURL)).conditions;
            this.valuesList = makeConditionValuesUnique(this.valuesList);
        }
        this.loadingConditionValues = false;
    }
    render(){
        return(
            <Grid container direction = "column" spacing=  {1}>
                <Grid item>
                    <Autocomplete
                        id = {"" + this.props.conditionKey}
                        inputValue = {this.props.conditions.conditionsList[this.props.conditionKey].condition}
                        options = {this.props.analyticsStateStore.allConditions}
                        getOptionLabel = {option => option[0]}
                        onChange = {this.handleConditionNameChange}
                        loading = {this.props.analyticsStateStore.loadingAllConditions}
                        style = {{width: "250px"}}
                        renderInput = {(params) => (
                            <TextField
                                {...params}
                                label = "Condition Name"
                                variant = "outlined"
                                onChange = {event => this.props.conditions.conditionsList[this.props.conditionKey].condition = event.target.value}
                                fullWidth
                            />
                        )}
                    />
                </Grid>

                <Grid item>
                    <Autocomplete
                        id = {"" + this.props.conditionKey}
                        inputValue = {this.props.conditions.conditionsList[this.props.conditionKey].value}
                        options = {this.valuesList}
                        getOptionLabel = {option => option[2]}
                        onChange = {(event, value) => {
                            if(value === null){
                                this.props.conditions.conditionsList[this.props.conditionKey].value = "";
                            }
                            else{
                                this.props.conditions.conditionsList[this.props.conditionKey].value = value[2];
                            }
                        }}
                        loading = {this.loadingConditionValues}
                        style = {{width: "250px"}}
                        renderInput = {(params) => (
                            <TextField
                                {...params}
                                label = "Condition Value"
                                variant = "outlined"
                                onChange = {event => this.props.conditions.conditionsList[this.props.conditionKey].value = event.target.value}
                                fullWidth
                            />
                        )}
                    />
                </Grid>
            </Grid>
        )
    }
}));


export default ConditionsInputField;