import { Button, Grid, TextField, Typography } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { getTwoExperiments } from "../../../config/api/getAPI";

const ChooseExperimentsForm = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class ChooseExperimentsForm extends React.Component{
    experimentName1 = "";
    experimentName2 = "";
    conditions = {
        conditionsList: []
    };
    conditionsTextFields = [];

    constructor(props){
        super(props);

        decorate(this, {
            conditionsTextFields: observable,
            experimentName1: observable,
            conditions: observable,
            experimentName2: observable
        })

    }

    handleMakeQuery = async () => {
        // Experiment fields can't be empty and they can't be equal
        if(this.experimentName2 === this.experimentName1){
            this.props.notificationStateStore.setErrorNotification("The same experiment was chosen twice. Please choose unique experiments.");
        }
        if(this.experimentName1 === ""){
            this.props.notificationStateStore.setErrorNotification("Error: experiment 1 is empty. Please choose an experiment");
        }
        if(this.experimentName2 === ""){
            this.props.notificationStateStore.setErrorNotification("Error: experiment 2 is empty. Please choose an experiment");
        }

        if(this.experimentName1 !== "" && this.experimentName2 !== "" && this.experimentName2 !== this.experimentName1) {
            this.props.analyticsStateStore.loadingExperiments = true;
            const data = await getTwoExperiments(this.experimentName1, this.experimentName2, this.props.appStateStore.backendURL);
            this.props.analyticsStateStore.setExperiments(data.experiments);
            this.props.analyticsStateStore.setLoadingExperiments(false);
            this.props.analyticsStateStore.setLoadedExperiments(true);
        }
    }

    render(){
        const items = this.conditionsTextFields.map((field) => {return field});
        return(
            <Grid container direction = "column" alignItems = "center" justify = "center" spacing = {2}>
                <Grid item>
                    <Typography>
                        First, choose two experiment names in order to compare them.
                    </Typography>
                </Grid>

                <Grid item container direction = "row" alignItems = "center" justify = "center" spacing = {2}>
                    <Autocomplete
                        id = "sequences"
                        inputValue = {this.experimentName1}
                        options = {this.props.analyticsStateStore.allExperiments}
                        getOptionLabel = {option => option}
                        onChange = {(event, value) => {
                            if(value === null){
                                this.experimentName1 = ""
                            }
                            else{
                                this.experimentName1 = value
                            }
                        }}
                        loading = {this.props.analyticsStateStore.loadingAllSequences}
                        style = {{width: "300px"}}
                        renderInput = {(params) => (
                            <TextField
                                {...params}
                                label = "Experiment Name"
                                onChange = {event => this.experimentName1 = event.target.value}
                                variant = "outlined"
                                fullWidth
                            />
                        )}
                    />
                    &nbsp;
                    {items}

                        <Autocomplete
                        id = "sequences"
                        inputValue = {this.experimentName2}
                        options = {this.props.analyticsStateStore.allExperiments}
                        getOptionLabel = {option => option}
                        onChange = {(event, value) => {
                            if(value === null){
                                this.experimentName2 = ""
                            }
                            else{
                                this.experimentName2 = value
                            }
                        }}
                        loading = {this.props.analyticsStateStore.loadingAllSequences}
                        style = {{width: "300px"}}
                        renderInput = {(params) => (
                            <TextField
                                {...params}
                                label = "Experiment Name"
                                onChange = {event => this.experimentName2 = event.target.value}
                                variant = "outlined"
                                fullWidth
                            />
                        )}
                    />

                    {items}
                </Grid>
                <Grid item>
                    <Button
                        color = "primary"
                        variant = "contained"
                        onClick = {this.handleMakeQuery}
                        disabled = {this.props.analyticsStateStore.loadingExperiments}
                        style = {{textTransform: "initial"}}
                    >
                        <SearchIcon/> &nbsp; Load Experiment Data
                    </Button>
                </Grid>
            </Grid>
        )
    }
}));


export default ChooseExperimentsForm;