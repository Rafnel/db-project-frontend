import { inject, observer } from "mobx-react";
import React from "react";
import { Button, Grid, IconButton, Tooltip, Typography } from "@material-ui/core";
import { decorate, observable } from "mobx";
import AddIcon from '@material-ui/icons/Add';
import MeasurementInputField from "../MeasurementInputField";
import SearchIcon from '@material-ui/icons/Search';
import { getMeasurementsFromExperiments } from "../../../config/api/getAPI";

export function MakeArrayUnique(array) {
	let temp = {};
	for (let i = 0; i < array.length; i++)
		temp[array[i]] = true;
	let r = [];
	for (let k in temp)
		r.push(k);
	return r;
}

const RetrieveMeasurements = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class ViewMultipleResultsExtraCred extends React.Component {
	measurements = [];
	measurementsTextFields = [];
	noMeasurementsEntered = false;

	constructor(props) {
		super(props);

		decorate(this, {
			measurementsTextFields: observable,
			measurements: observable,
			noMeasurementsEntered: observable
		});

		//initially only one text field.
		this.handleAddMeasurement();
	}

	handleAddMeasurement = () => {
		this.measurements.push({ name: "" });
		const measurementKey = this.measurementsTextFields.length;
		this.measurementsTextFields.push(
			<Grid key={this.measurementsTextFields.length} item>
				<MeasurementInputField measurementKey={measurementKey} measurements={this.measurements} />
			</Grid>
		)
	}

	handleStoreRows = async () => {
		this.props.analyticsStateStore.setLoadedTableExperiments(false);
		var experiment_ids = [];
		const experiments = this.props.analyticsStateStore.experiments;
		for (let i = 0; i < experiments.length; i++) {
			experiment_ids.push(experiments[i].experiment_id);
		}
		this.props.analyticsStateStore.setLoadingExperiments(true);

		var measurement_names = MakeArrayUnique(this.measurements.map(item => item.name));
		experiment_ids = MakeArrayUnique(experiment_ids);

		if(measurement_names.length === 1 && measurement_names[0] === ""){
			this.props.notificationStateStore.setErrorNotification("Error: No Measurements Selected");
		}

		if(experiment_ids.length === 0){
			this.props.notificationStateStore.setErrorNotification("Error: No Experiments Selected.  Return to this page once you have selected experiments");
		}

		this.props.analyticsStateStore.setMeasurements(measurement_names);

		const data = await getMeasurementsFromExperiments(experiment_ids, measurement_names, this.props.appStateStore.backendURL);
		this.props.analyticsStateStore.setTableExperiments(data.experiments);
		this.props.analyticsStateStore.setLoadingExperiments(false);
		this.props.analyticsStateStore.setLoadedTableExperiments(true);
		this.forceUpdate();
	}

	render() {
		const measurements = this.measurementsTextFields.map((field) => {
			return field
		});
		return (
			<Grid container direction="row" alignItems="center" justify="center" spacing={2}>
				<Grid item>
					<Typography>
						First, enter the set of measurements that make up your experiment.
                    </Typography>
				</Grid>
				<Grid item container direction="row" alignItems="flex-start" justify="flex-start" spacing={2}>
					{measurements}
					<Grid item>
						<Tooltip title="Add Measurement">
							<IconButton color="primary" onClick={this.handleAddMeasurement}>
								<AddIcon />
							</IconButton>
						</Tooltip>
					</Grid>
				</Grid>
				<Grid item>
					<Button
						color="primary"
						variant="contained"
						onClick={this.handleStoreRows}
						disabled={this.props.analyticsStateStore.loadingExperiments}
						style={{ textTransform: "initial" }}
					>
						<SearchIcon /> &nbsp; Load Experiment Data
						</Button>
				</Grid>
			</Grid>
		);
	}
}));
export default RetrieveMeasurements;