import { Button, Grid, IconButton, Tooltip, Typography } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import { getExtraCredExperiments } from "../../../config/api/getAPI";
import ConditionsInputField from "../ConditionsInputField";
import SequencesInputField from "../SequencesInputField";

const RetrieveExperimentsWithCriteria = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(
	class RetrieveExperimentsWithCriteria extends React.Component {
		sequences = [];
		conditions = {
			conditionsList: []
		};
		sequencesTextFields = []
		conditionsTextFields = []

		constructor(props) {
			super(props);

			decorate(this, {
				sequencesTextFields: observable,
				conditionsTextFields: observable,
				sequences: observable,
				conditions: observable
			})

			//initially only one text field.
			this.handleAddCondition();
			this.handleAddSequence();
		}

		handleAddCondition = () => {
			this.conditions.conditionsList.push({ condition: "", value: "" });
			const conditionKey = this.conditionsTextFields.length;
			this.conditionsTextFields.push(
				<Grid key={this.conditionsTextFields.length} item>
					<ConditionsInputField conditionKey={conditionKey} conditions={this.conditions} />
				</Grid>
			)
		}

		handleAddSequence = () => {
			this.sequences.push({ name: "" });
			const sequenceKey = this.sequencesTextFields.length;
			this.sequencesTextFields.push(
				<Grid key={this.sequencesTextFields.length} item>
					<SequencesInputField sequenceKey={sequenceKey} sequences={this.sequences} />
				</Grid>
			)
		}

		handleMakeQuery = async () => {
			this.props.analyticsStateStore.loadingExperiments = true;
			let emptyConditionCounter = 0, emptyValueCounter = 0;
			let invalidPair = false;
			this.conditions.conditionsList.forEach(item => {
				if (item.condition === "") {
					emptyConditionCounter++;
				}
				if (item.value === "") {
					emptyValueCounter++;
				}
				if ((item.condition === "" && item.value !== "") || (item.condition !== "" && item.value === "")) {
					invalidPair = true;
				}
			})
			if (emptyConditionCounter == emptyValueCounter && !invalidPair) {
				const conditions = this.conditions.conditionsList.filter((item) => item.condition !== "" && item.value !== "");
				const sequence_names = this.sequences.map(item => item.name);
				const condition_names = conditions.map(item => item.condition);
				const condition_values = conditions.map(item => item.value);
				if (condition_names.length === condition_values.length) {
					const data = await getExtraCredExperiments(sequence_names, condition_names, condition_values, this.props.appStateStore.backendURL);
					this.props.analyticsStateStore.setExperiments(data.experiments);
					this.props.analyticsStateStore.setLoadingExperiments(false);
					this.props.analyticsStateStore.setLoadedExperiments(true);
				} else {
					//TODO: ERROR POPUP
					//TODO: MAKE THIS CHECK WORK
				}
			} else {
				this.props.notificationStateStore.setErrorNotification("One or more condition/value pairs is incorrect.");
				this.props.analyticsStateStore.loadingExperiments = false;

			}
		}

		render() {
			const items = this.conditionsTextFields.map((field) => { return field });
			const sequences = this.sequencesTextFields.map((field) => { return field });
			return (
				<Grid container direction="row" alignItems="center" justify="center" spacing={2}>
					<Grid item>
						<Typography>
							First, enter the set of sequences that make up your experiment.
						</Typography>
					</Grid>

					<Grid item container direction="row" alignItems="flex-start" justify="flex-start" spacing={2}>
						{sequences}
						<Grid item>
							<Tooltip title="Add Sequence">
								<IconButton color="primary" onClick={this.handleAddSequence}>
									<AddIcon />
								</IconButton>
							</Tooltip>
						</Grid>
					</Grid>

					<Grid item>
						<Typography>
							Second, enter the set of conditions that make up your experiment.
						</Typography>
					</Grid>

					<Grid item container direction="row" alignItems="flex-start" justify="flex-start" spacing={2}>
						{items}
						<Grid item>
							<Tooltip title="Add Condition">
								<IconButton color="primary" onClick={this.handleAddCondition}>
									<AddIcon />
								</IconButton>
							</Tooltip>
						</Grid>
					</Grid>

					<Grid item>
						<Button
							color="primary"
							variant="contained"
							onClick={this.handleMakeQuery}
							disabled={this.props.analyticsStateStore.loadingExperiments}
							style={{ textTransform: "initial" }}
						>
							<SearchIcon /> &nbsp; Load Experiment Data
						</Button>
					</Grid>
				</Grid>
			)
		}
	}));


export default RetrieveExperimentsWithCriteria;