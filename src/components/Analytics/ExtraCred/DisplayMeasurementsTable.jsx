import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from 'react';

function createData(name, calories, fat, carbs, protein) {
	return { name, calories, fat, carbs, protein };
}

const ConsoleLog = ({ children }) => {
	return false;
};

const MeasurementsTable = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class MeasurementsTable extends React.Component {

	experiment_ids = [];
	measurement_names = [];
	header = [];
	row = "";

	constructor(props) {
		super(props);

		decorate(this, {
			sequencesTextFields: observable,
			conditionsTextFields: observable,
			sequences: observable,
			conditions: observable,
			measurement_names: observable,
			header: observable,
			row: observable
		});
		//this was fine
		// this.createHeadings();
	}

	componentDidMount() {
		this.createHeadings();
	}

	//this was all fine
	createHeadings() {
		let temp = [];
		for (let i = 0; i < this.props.analyticsStateStore.measurements.length; i++) {
			temp.push(<TableCell key={i} align="right">{this.props.analyticsStateStore.measurements[i]}</TableCell>);
		}
		this.header = temp;
		this.forceUpdate();
	}

	render() {
		return (
			<div>
				<Paper >
					<Table aria-label="simple table">
						<TableHead>
							<TableRow>
								<TableCell>Measurement</TableCell>
								{this.header}
							</TableRow>
						</TableHead>
						<TableBody>
							{this.props.analyticsStateStore.tableExperiments.map(row => (
								<TableRow key={row.experiment_id}>
									<TableCell component="th" scope="row">
										{row.experiment_id}
									</TableCell>
									{row.measurements.map(row2 => (
										<TableCell align="right"> {row2.value}</TableCell>
									))}
								</TableRow>
							))}
						</TableBody>
					</Table>
				</Paper>
			</div>
		);
	}
}))

export default MeasurementsTable;