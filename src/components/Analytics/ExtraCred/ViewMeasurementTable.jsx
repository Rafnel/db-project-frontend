import { Grid } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React, { Fragment } from "react";
import MeasurementsTable from "./DisplayMeasurementsTable";
import RetrieveMeasurements from "./RetrieveMeasurements";

const ViewMeasurementsTable = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class ViewMultipleResultsExtraCred extends React.Component {

	render() {
		return (
			<Fragment>
				<Grid item>
					<RetrieveMeasurements/>
				</Grid>

				{(this.props.analyticsStateStore.loadedTableExperiments) &&
					<Grid style={{width: "100%"}} item>
						<MeasurementsTable/>
					</Grid>
				}
			</Fragment>
		)
	}
}));


export default ViewMeasurementsTable;