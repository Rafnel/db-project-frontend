import { Grid } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React, { Fragment } from "react";
import ExperimentQueryResults from "../ExperimentQueryResults";
import RetrieveExperimentsWithCriteria from './RetrieveExperimentsWithCriteria';

const ViewMultipleResultsExtraCred = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class ViewMultipleResultsExtraCred extends React.Component {
	render() {
		return (
			<Fragment style={{ width: "100%" }}>
				<Grid item>
					<RetrieveExperimentsWithCriteria/>
				</Grid>

				{(this.props.analyticsStateStore.loadingExperiments || this.props.analyticsStateStore.loadedExperiments) &&
					<Grid style={{ width: "100%" }} item>
						<ExperimentQueryResults/>
					</Grid>
				}
			</Fragment>
		)
	}
}));


export default ViewMultipleResultsExtraCred;