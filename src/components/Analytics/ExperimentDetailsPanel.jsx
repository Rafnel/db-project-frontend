import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Grid, Typography, Card, CardHeader, CardContent } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { inject, observer } from "mobx-react";
import React from "react";
import { SECONDARY } from "../..";


const ExperimentDetailsPanel = inject("analyticsStateStore")(observer(class ExperimentDetailsPanel extends React.Component{
    renderExperimentConditions = () => {
        let conditionComponents = [];

        for(let i = 0; i < this.props.experiment.conditions.length; i++){
            conditionComponents.push(
                <Grid key = {i} item>
                    {this.props.experiment.conditions[i][0] + ": " + this.props.experiment.conditions[i][1]}
                </Grid>
            )
        }

        return conditionComponents;
    }

    renderExperimentMeasurements = () => {
        let measurementComponents = [];

        for(let i = 0; i < this.props.experiment.measurements.length; i++){
            measurementComponents.push(
                <Grid key = {i} item>
                    {this.props.experiment.measurements[i][0] + ": " + this.props.experiment.measurements[i][1]}
                </Grid>
            )
        }

        return measurementComponents;
    }

    render(){
        return(
            <Card style = {{backgroundColor: "#d9d9d9", width: "100%", height: "45vh", overflow: "auto"}}>
                    <Grid style = {{width: "100%", padding: 10}} container direction = "column" alignItems = "flex-start" justify = "center" spacing = {1}>
                        <Grid item>
                            <Typography variant = "h6">
                                {this.props.experiment.experiment_id}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography>
                                <b>Conditions:</b>
                            </Typography>
                        </Grid>

                        {this.renderExperimentConditions()}

                        <Grid item>
                            <Typography>
                                <b>Measurements:</b>
                            </Typography>
                        </Grid>

                        {this.renderExperimentMeasurements()}
                    </Grid>
            </Card>
        )
    }
}));


export default ExperimentDetailsPanel;