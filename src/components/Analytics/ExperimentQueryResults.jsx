import { Grid, Paper, Typography } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React from "react";
import BeatLoader from 'react-spinners/BeatLoader';
import { PRIMARY } from "../..";
import ExperimentDetailsPanel from "./ExperimentDetailsPanel";

const ExperimentQueryResults = inject("appStateStore", "notificationStateStore", "analyticsStateStore")(observer(class ExperimentQueryResults extends React.Component{
    renderExperimentPanels = () => {
        let panels = [];

        const experiments = this.props.analyticsStateStore.experiments;
        for(let i = 0; i < experiments.length; i++){
            panels.push(
                <Grid style = {{width: "50%"}} key = {i} item>
                    <ExperimentDetailsPanel experiment = {experiments[i]}/>
                </Grid>
            );
        }

        if(panels.length === 0){
            panels.push(
                <Grid key = {0} item>
                    <Typography>
                        An experiment with this set of (sequence, conditions) does not exist.
                    </Typography>
                </Grid>
            )
        }

        return panels;
    }
    render(){
        return(
            <Paper style = {{padding: 10, width: "100%"}}>
                <Grid container direction = "column" alignItems = "center" justify = "center" spacing = {2}>
                    <Grid item>
                        <Typography variant = "h6">
                            Results
                        </Typography>
                    </Grid>

                    {this.props.analyticsStateStore.loadingExperiments && 
                        <Grid item>
                            <BeatLoader size = {20} color = {PRIMARY}/> 
                        </Grid>
                    }

                    {!this.props.analyticsStateStore.loadingExperiments && this.props.analyticsStateStore.loadedExperiments && 
                        <Grid style = {{width: "100%"}} item container direction = "row" alignItems = "flex-start" justify = "center" spacing = {1}>
                            {this.renderExperimentPanels()}
                        </Grid>
                    }
                </Grid>
            </Paper>
        )
    }
}));


export default ExperimentQueryResults;