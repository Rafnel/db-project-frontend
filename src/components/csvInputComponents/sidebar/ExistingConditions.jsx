import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import BeatLoader from 'react-spinners/BeatLoader';
import { PRIMARY } from "../../..";
import { getAllConditions } from "../../../config/api/getAPI";


const ExistingConditions = inject("appStateStore", "csvStateStore")(observer(class ExistingConditions extends React.Component{
    loadingConditions = true;

    constructor(props){
        super(props);

        decorate(this, {
            loadingConditions: observable,
        })
    }
    render(){
        const conds = this.props.csvStateStore.conditions.map(item => <div style = {{marginBottom: 10}} key = {item[0]}><Typography><b>{item[0]}</b></Typography></div>);
        return(
            <ExpansionPanel style = {{width: "20vw", backgroundColor: "#d9d9d9"}} defaultExpanded>
                <ExpansionPanelSummary expandIcon = {<ExpandMoreIcon/>}>
                    Existing Conditions
                </ExpansionPanelSummary>

                {!this.loadingConditions &&
                    <ExpansionPanelDetails>
                        <div style = {{display: "flex", flexDirection: "column"}}>
                            {conds}
                        </div>
                    </ExpansionPanelDetails>
                }

                {this.loadingConditions &&
                    <ExpansionPanelDetails>
                        <BeatLoader color = {PRIMARY}/>
                    </ExpansionPanelDetails>
                }
            </ExpansionPanel>
        )
    }

    async componentDidMount(){
        const conds = await getAllConditions(this.props.appStateStore.backendURL);
        this.props.csvStateStore.conditions = conds.conditions;
        this.loadingConditions = false;
    }
}));


export default ExistingConditions;