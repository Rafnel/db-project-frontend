import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import BeatLoader from 'react-spinners/BeatLoader';
import { PRIMARY } from "../../..";
import { getAllMeasurements } from "../../../config/api/getAPI";


const ExistingMeasurements = inject("appStateStore", "csvStateStore")(observer(class ExistingMeasurements extends React.Component{
    loadingMeasurements = true;

    constructor(props){
        super(props);

        decorate(this, {
            loadingMeasurements: observable
        })
    }
    render(){
        const measurements = this.props.csvStateStore.measurements.map(item => <div style = {{marginBottom: 10}}><Typography><b>{item[0]}</b></Typography></div>);
        return(
            <ExpansionPanel style = {{width: "20vw", backgroundColor: "#d9d9d9"}} defaultExpanded>
                <ExpansionPanelSummary expandIcon = {<ExpandMoreIcon/>}>
                    Existing Measurements
                </ExpansionPanelSummary>

                {!this.loadingMeasurements &&
                    <ExpansionPanelDetails>
                        <div style = {{display: "flex", flexDirection: "column"}}>
                            {measurements}
                        </div>
                    </ExpansionPanelDetails>
                }

                {this.loadingMeasurements &&
                    <ExpansionPanelDetails>
                        <BeatLoader color = {PRIMARY}/>
                    </ExpansionPanelDetails>
                }
            </ExpansionPanel>
        )
    }

    async componentDidMount(){
        const measures = await getAllMeasurements(this.props.appStateStore.backendURL);
        this.props.csvStateStore.measurements = measures.measurements;
        this.loadingMeasurements = false;
    }
}));


export default ExistingMeasurements;