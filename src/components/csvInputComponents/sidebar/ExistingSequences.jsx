import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { decorate, observable } from "mobx";
import { inject, observer } from "mobx-react";
import React from "react";
import BeatLoader from 'react-spinners/BeatLoader';
import { PRIMARY } from "../../..";
import { getAllMeasurements, getAllSequences } from "../../../config/api/getAPI";


const ExistingSequences = inject("appStateStore", "csvStateStore")(observer(class ExistingSequences extends React.Component{
    loadingSequences = true;

    constructor(props){
        super(props);

        decorate(this, {
            loadingSequences: observable
        })
    }
    render(){
        const sequences = this.props.csvStateStore.sequences.map(item => <div style = {{marginBottom: 10}}><Typography><b>{item[0]}</b></Typography></div>);
        return(
            <ExpansionPanel style = {{width: "20vw", backgroundColor: "#d9d9d9"}} defaultExpanded>
                <ExpansionPanelSummary expandIcon = {<ExpandMoreIcon/>}>
                    Existing Sequences
                </ExpansionPanelSummary>

                {!this.loadingSequences &&
                    <ExpansionPanelDetails>
                        <div style = {{display: "flex", flexDirection: "column"}}>
                            {sequences}
                        </div>
                    </ExpansionPanelDetails>
                }

                {this.loadingSequences &&
                    <ExpansionPanelDetails>
                        <BeatLoader color = {PRIMARY}/>
                    </ExpansionPanelDetails>
                }
            </ExpansionPanel>
        )
    }

    async componentDidMount(){
        const seqs = await getAllSequences(this.props.appStateStore.backendURL);
        this.props.csvStateStore.sequences = seqs.sequences;
        this.loadingSequences = false;
    }
}));


export default ExistingSequences;