import { Grid, Typography } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React, { Fragment } from "react";
import BeatLoader from 'react-spinners/BeatLoader';
import { PRIMARY } from "../../..";
import CreatedExperimentChip from "./CreatedExperimentChip";
import FailedToCreateChip from "./FailedToCreateChip";


const CSVEnteredResults = inject("notificationStateStore", "csvStateStore", "appStateStore")(observer(class CSVEnteredResults extends React.Component{
    renderResults = () => {
        let experimentsAddedChips = [];
        let experimentsFailedToAdd = [];
        let measurementValuesCreated = [];
        let measurementsRejected = [];

        const results = this.props.resultsObject.data;

        for(let i = 0; i < results.experiments_created.length; i++){
            experimentsAddedChips.push(
                <Grid key = {i} item>
                    <CreatedExperimentChip chipTitle = {results.experiments_created[i]}/>
                </Grid>
            )
        }

        for(let i = 0; i < results.invalid_experiments.length; i++){
            experimentsFailedToAdd.push(
                <Grid key = {i} item>
                    <FailedToCreateChip chipTitle = {"Experiment: \"" + results.invalid_experiments[i].experiment_name + "\" Reason: " + results.invalid_experiments[i].reason}/>
                </Grid>
            )
        }

        for(let i = 0; i < results.measurement_values_created.length; i++){
            measurementValuesCreated.push(
                <Grid key = {i} item>
                    <CreatedExperimentChip chipTitle = {"Measurement \"" + results.measurement_values_created[i].measurement + "\" was assigned value \"" + results.measurement_values_created[i].measurement_value + "\" for experiment " + results.measurement_values_created[i].experiment_id}/>
                </Grid>
            )
        }

        for(let i = 0; i < results.invalid_measurements.length; i++){
            measurementsRejected.push(
                <Grid key = {i} item>
                    <FailedToCreateChip chipTitle = {"Measurement: \"" + results.invalid_measurements[i].measurement + "\" Reason: " + results.invalid_measurements[i].reason}/>
                </Grid>
            )
        }

        return(
            <Fragment>
                <Grid item>
                    <Typography>
                        <b>Experiments Created</b>
                    </Typography>
                </Grid>

                {experimentsAddedChips}

                <Grid item>
                    <Typography>
                        <b>Experiments Rejected</b>
                    </Typography>
                </Grid>

                {experimentsFailedToAdd}

                <Grid item>
                    <Typography>
                        <b>Measurements Values Created</b>
                    </Typography>
                </Grid>

                {measurementValuesCreated}

                <Grid item>
                    <Typography>
                        <b>Measurements Rejected</b>
                    </Typography>
                </Grid>

                {measurementsRejected}
            </Fragment>
        )
    }
    render(){
        const resultsHeight = window.innerHeight - this.props.appStateStore.appBarHeight - this.props.csvStateStore.csvInputComponentHeight;
        return(
            <div style = {{width: "100%", height: resultsHeight, paddingTop: 15, overflowY: "scroll", overflowX: "hidden"}}>
                <Grid container direction = "column" alignItems = "center" justify = "center" spacing = {2}>
                    <Grid item>
                        <Typography variant = "h6">
                            Results
                        </Typography>
                    </Grid>

                    {this.props.resultsObject.loading && 
                        <Grid item>
                            <BeatLoader size = {20} color = {PRIMARY}/> 
                        </Grid>
                    }

                    {!this.props.resultsObject.loading && this.renderResults()}
                    
                </Grid>
            </div>
        )
    }
}));

export default CSVEnteredResults;