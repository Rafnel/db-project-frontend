import { inject, observer } from "mobx-react";
import React from "react";
import { Chip } from "@material-ui/core";


const FailedToCreateChip = inject("notificationStateStore")(observer(class FailedToCreateChip extends React.Component{
    render(){
        return(
            <Chip label = {this.props.chipTitle} color = "default"/>
        )
    }
}));

export default FailedToCreateChip;