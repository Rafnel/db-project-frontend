import { Chip } from "@material-ui/core";
import { inject, observer } from "mobx-react";
import React from "react";


const CreatedExperimentChip = inject("notificationStateStore")(observer(class CreatedExperimentChip extends React.Component{
    render(){
        return(
            <Chip label = {this.props.chipTitle} color = "primary"/>
        )
    }
}));

export default CreatedExperimentChip;