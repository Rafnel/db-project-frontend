import { Button, Divider, Grid, Typography } from "@material-ui/core";
import SaveIcon from '@material-ui/icons/Save';
import { inject, observer } from "mobx-react";
import React from "react";
import CSVReader from 'react-csv-reader';
import { importCSVData } from "../../../config/api/createAPI";


const InputCSV = inject("csvStateStore", "notificationStateStore", "appStateStore")(observer(class InputCSV extends React.Component{
    processCSV = (data) => {
		this.props.csvStateStore.fileUploaded = data;
    }

    uploadCSV = async () => {
        if(this.props.csvStateStore.fileUploaded.length === 0){
            this.props.notificationStateStore.setErrorNotification("Please enter a CSV file with data in it. Your file has no data.");
            return;
        }
        if(this.props.csvStateStore.fileUploaded[this.props.csvStateStore.fileUploaded.length - 1].length === 1 && this.props.csvStateStore.fileUploaded[this.props.csvStateStore.fileUploaded.length - 1][0] === ""){
            //check that each row has same number of columns.
            let len = -1;
            for(let i = 0; i < this.props.csvStateStore.fileUploaded.length - 1; i++){
                if(len === -1){
                    len = this.props.csvStateStore.fileUploaded[i].length;
                }
                else{
                    if(len !== this.props.csvStateStore.fileUploaded[i].length){
                        this.props.notificationStateStore.setErrorNotification("Your CSV file should have an equal number of columns in each row of data.");
                        return;
                    }
                }
            }
        }
        else{
            //check that each row has same number of columns.
            let len = -1;
            for(let i = 0; i < this.props.csvStateStore.fileUploaded.length; i++){
                if(len === -1){
                    len = this.props.csvStateStore.fileUploaded[i].length;
                }
                else{
                    if(len !== this.props.csvStateStore.fileUploaded[i].length){
                        this.props.notificationStateStore.setErrorNotification("Your CSV file should have an equal number of columns in each row of data.");
                        return;
                    }
                }
            }
        }
        this.props.csvStateStore.resultsObject.loading = true;
		const response = await importCSVData(this.props.appStateStore.backendURL, this.props.csvStateStore.fileUploaded);
		// const response = {
		// 	data: {
		// 		invalid_experiments: [],
		// 		invalid_conditions: [],
		// 		invalid_measurements: [],
		// 		experiments_created: [],
		// 		measurement_values_created: []
		// 	}
		// }
		this.props.csvStateStore.resultsObject.data = response.data;

		if(!response.success){
			this.props.notificationStateStore.setErrorNotification(response.data);
		}

		this.props.csvStateStore.resultsObject.loading = false;
		this.props.csvStateStore.finishedUpload = true;

	}
    
    render(){
        return(
            <Grid ref = "csvInput" container direction = "column" alignItems = "center" justify = "center" spacing = {2} style = {{paddingTop: "20px"}}>
                <Grid item>
                    <Typography variant = "h5">
                        Enter your data with a file
                    </Typography>
                </Grid>

                <Grid item>
                    <Typography variant = "body1">
                        On this page, you can enter your data with a .csv file. This is a file with comma-separated values.
                        <br/>The top-left cell in your CSV must be empty to be processed correctly. Each row in the file must have an equal
                        number of columns.
                        <br/>Any sequence / condition / measurement referenced in the CSV file must exist already to be accepted.
                        <br/>To add new sequences / conditions / measurements easily, refer to the sidebar.
                    </Typography>
                </Grid>

                <Grid item>
                    <CSVReader
                        onFileLoaded = {this.processCSV}
                    />
                </Grid>

                <Grid item>
                    <Button
                        variant = "contained"
                        disabled = {this.props.csvStateStore.fileUploaded === null || this.props.csvStateStore.resultsObject.loading}
                        onClick = {this.uploadCSV}
                        color = "primary"
                    >
                        <SaveIcon/> &nbsp; Upload
                    </Button>
                </Grid>

                <Grid item>
                    <Divider style = {{width: "76vw"}}/>
                </Grid>
            </Grid>
        )
    }

    componentDidMount(){
        let {clientHeight, clientWidth} = this.refs.csvInput;

        this.props.csvStateStore.csvInputComponentHeight = clientHeight;
    }
}));

export default InputCSV;